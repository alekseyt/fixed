[TOC]

## What is this

Fixed point math library for C++20 designed to prevent unintended
information destruction (or creation).

Some other fixed-point libraries usually allocate bits for integer and
fractional parts and fixed-point precision means how many bits are
allocated for fractional (or integer) part.

In `fixed` precision means how many decimal places are allocated for
fractional part. E.g. `fixed::p<3>` means 3 decimal places as in `1.000`.

This is more a "money type" than "floating point type replacement for
no-FPU systems".

If you're looking for other fixed-point math libraries, take a look
at [libfixmath][], [fp][], [fpm][] or [CNL][].

[libfixmath]: https://github.com/PetteriAimonen/libfixmath
[fp]: https://github.com/mizvekov/fp
[fpm]: https://mikelankamp.github.io/fpm/
[CNL]: https://github.com/johnmcfarlane/cnl

## Status

Highly experimental

## Compiler support

* GCC 10.2

## Internals

* `fixed::p` (`fixed::point`) - fixed point number
* `fixed::f` (`fixed::fraction`) - fractional part of fixed point number.
  As a matter of fact, this is `fixed::p` with integer part always set to
  zero. It doesn't represent anything other than fractional part.
* `fixed::r` (`fixed::ratio`) - for now it's a ratio that incorporates two
  `fixed::p`s: dividend and divider. It is an unevaluated fraction that might
  be evaluated back to `fixed::p` with arbitrary precision.

## How to use

```c++
const auto x = fixed::make::p<2>(1, 11);
```

`x` is going to be `std::optional<fixed::p<2>>`.

`make::p` notation losely follows floating point notation, `(1, 11)`
would mean `1.11` in floating-point notation.

Note that `fixed::make::p<2>(1, 1)` would produce number similar to
floating-point `1.10` (`1.1`) instead of `1.01`. To make `1.01`:

```c++
const auto x = fixed::make::p<2>(1, fixed::make::f<1>(1));
```

In latter example `fixed::p` has two decimal places as in `1.00`,
but `fixed::f` only one decimal place as in `0.01` *loosely*. When
`fixed::f` is combined with `fixed::p`, it assumes that `fixed::f` is
already scaled to required precision, `1` scaled to two decimal places
is `0.01`.

## Supported operations

### Base operations

* `fixed::add()` - addition
* `fixed::sub()` - subtraction
* `fixed::mul()` - multiplication
* `fixed::div()` - division

Division support arbitrary precision as long as there are enough bits.
For instance `1/3` would produce fraction `0.333333...` which would never be
accurate, however it can be retrieved as `fixed::p` with precision that is
supported by platform.

Every operation is accompanied by corresponding operator. For example
`a + b` will be translated into `add(a, b)` where `a` and `b` are `fixed::p`s.

Note that `a / b` will return `fixed::r` similarly to `div()`.

Most operators also support optional operands and return optional result.

### Transformations

* `fixed::upscale()` - transform `fixed::p` to higher precision
* `fixed::downscale()` - transform `fixed::p` to lower precision

### Utilities

* `fixed::precision()` - establish precision of `fixed::p`. This is
  compile-time parameter of any number.
* `fixed::is_accurate()` - establish if `fixed::p` is accurate (more on
  that below). This is runtime parameter of any number.
* `fixed::accuracy()` - establish effective accuracy of a number. This is
  runtime parameter of any number.
* `fixed::to_string()` - convert `fixed::p` to string representation

## Precision control

`fixed::p` with specific precision can not be directly assigned to `fixed::p`
with another precision regardless if precision is lower or higher.

This is due to the fact(s) that upscaling to higher precision might
overflow and downscaling might loose accuracy. Therefore every `fixed::p`
is need to be explicitly upscaled or downscaled, explicitly checking
result of operation.

### Compile-time precision checks

Ability to set desired precision is controlled at compile time:

```c++
fixed::make::p<3, uint8_t>((uint8_t)0, 0);
```

Is the compile error because `uint8_t` can only represent two decimal
places.

By extension, precision checks cover operations, for instance multiplying
two numbers will increase their precision therefore:

```c++
fixed::make::p<1, uint8_t>((uint8_t)0, 0) * fixed::make::p<2, uint8_t>((uint8_t)0, 0);
```

Is also a compile error because result would have three decimal places
that can not be represented in `uint8_t`.

```
note: constraints not satisfied
...
note: the expression '(can_scale_to_precision<ResultStorageT, TargetPrecision>)() [with ResultStorageT = unsigned char; TargetPrecision = 3]' evaluated to 'false'
```

## Accuracy control

There are couple of cases when `fixed::p` might loose accuracy:

* Downscaling it to lower precision as if `1.11` would be trimmed to `1.1`
  and `0.01` would be lost

Note that trimming `1.10` to `1.1` doesn't loose accuracy and the same
is true when downscaling `fixed::p<2>(1, 10)` to one decimal
place, this won't produce inaccurate `fixed::p<1>`.

* If source number is inaccurate. This might happen in the following
  cases:

1. Explicitly telling `fixed::make::p()` that number is inaccurate
2. When result of operation is not accurate, for instance dividing `1/3`
   produces `0.3333...` and that is never accurate

Operation result will "inherit" inaccuracy of operands, if any. For
example, adding accurate and inaccurate numbers will produce inaccurate
number.

### Effective accuracy

Note that even if a number is stored with 3 decimal places, effective accuracy
might be lower if inaccurate numbers were used during calculations.

Accuracy will be inherited during operations and will be capped to lower
accuracy of operands. For instance if numbers with precision `2` and `3` are
added, and one of the numbers is inaccurate, result accuracy will be `2`.

Note that precision of the result will still be maintained and will be `3`.

## Comparison

`fixed` does provide comparison operator, however some numbers might be
unordered. When accurate number is compared with inaccurate number, some
relation might be established, for instance accurate `a` might be less
or greater than inaccurate `b`.

Equality might not be always established, even if two numbers are equal
in underlying representation, they are going to be unordered if one of them
is inaccurate.

## Error handling

*Every* operation might throw exception. This is a requirement, there is
no option to disable exceptions.

Exceptions are listed in `exceptions.hpp` and base on `fixed::exception`
which is `std::exception` in disguise.

Generally expect that everything in `fixed` namespace might throw.

Taxonometry of exceptions is not very important and subject to change. Choice
of exceptions over optionals is due to desire to keep `fixed` more in the
spirit of C++ rather than some other languages.

## Type conversions

For now only `fixed::to_string()` is provided to convert `fixed::p`
to string.

Convertions to other types is not available because converting to
floating-point number might loose accuracy, internal representation won't
make much sense, extracting parts from `fixed::p` is error-prone due to
risk of loosing leading zeroes, etc.

## Operations on ratios

For ratios same operations set is provided:

* `fixed::add()` - addition
* `fixed::sub()` - subtraction
* `fixed::mul()` - multiplication
* `fixed::div()` - division

They are implemented as "higher-level" operations that work with `fixed::r`'s
dividend and divider.

Note that since internally multiplication and addition (subtraction) on
`fixed::p` is used to implement those operations, precision of the result
is going to change according to usual rules that are applicable to operations
on `fixed::p`.

By default, ratios are going to be simplified, i.e. `6/10` -> `3/5`.

`1/3` + `1/3` + `1/3` should produce accurate `1`, with regard that precision
of the result's divider is going to be the sum of precisions of `3`'s from
operands.

`1` - `1/3` - `1/3` - `1/3` should produce accurate `0` (zero).

Precision of the result isn't simplified due to it being compile-time property,
however, result might be `fixed::dowscale()`-ed if needed. If resulting value
can be trimmed loselessly, downscaled value should also be accurate.

## Optimizations

Not in the scope of this implementation. Main focus of this library is on
the principal design, not on performance. You can safely assume that library
isn't optimized for performance.

Couple notes though:

* `add()`, `sub()` and `mul()` should be about as fast as adding,
  subtracting or multiplying integer numbers + optionals overhead
  (if any) + branching + function calls
* `div()` is slow, more precision - slower, less precision - faster
* All numbers are virtually immutable, every operation will (might)
  produce a new instance of a number holding operation result, therefore
  memory copying
* Comparison use temporaries and operands might be upscaled to common
  precision
* Since ratio operations normally implemented as higher-level algorithms
  on base operations with `fixed::p`, they imply complexity of several
  base operations + ratio simplification overhead (by default)

## Examples

Basic operations: [basic.cpp][]

[basic.cpp]: https://bitbucket.org/alekseyt/fixed/src/master/examples/basic.cpp

## Things to look into

* Rounding: `trim()` (`floor()`), `ceil()`, `round()`
* Better interface for `fixed::make::p()` in regard to passing `fixed::f`
  with non-default precision
* Should `fixed::p`'s `Storage` require `std::integral`? Ideally fixed
  would be compatible with integral-like user types, not just trivial
  integers. For that fixed has to depend on minimal set of arithmetic
  operations. It's also unclear if overflow-control built-ins are going
  to be compatible with types that overload operators, probably not, but
  maybe specialization of overflow control might be provided for user
  types.
* Revisit `consteval` and `noexcept` usages

## Implementation notes

* There is a side-effect when precision grow during more or less complicated
  computations, for instance, in ratios arithmetics. That, in a sense,
  gets in the way, however, having precision as a compile-time parameter
  of a number has its own benefits. Also, making it run-time parameter might
  effectively change fixed-point numbers into floating-point numbers.
  Therefore, precision is going to stay compile-time parameter, for now,
  perhaps it is worth to explore this further.
* Exceptions seems to be more convenient way of error handling, as they do
  not require explicit result checking, especially in expressions like
  `a + b * c`. When optionals are used, error might get "lost" somewhere
  in the middle of expression, but exception would interrupt computation.

## TODO

* Modularize
* Doxygen doc
