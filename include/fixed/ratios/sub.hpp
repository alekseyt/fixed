#ifndef FIXED_RATIOS_SUB_HPP
#define FIXED_RATIOS_SUB_HPP

#include <fixed/operations/div.hpp>
#include <fixed/operations/mul.hpp>
#include <fixed/operations/sub.hpp>
#include <fixed/ratio.hpp>
#include <fixed/ratios/add.hpp>

namespace fixed {

template <detail::is_fixed_r A, detail::is_fixed_r B>
detail::is_fixed_r auto sub(const A &a, const B &b, bool do_simplify=true)
{
	const detail::is_fixed_p auto dividend =
		sub(mul(a.dividend, b.divider), mul(b.dividend, a.divider));
	const detail::is_fixed_p auto divider = mul(a.divider, b.divider);

	return detail::transform_ratio_operation_result(div(dividend, divider),
		do_simplify);
}

} // namespace fixed

#endif // FIXED_RATIOS_SUB_HPP
