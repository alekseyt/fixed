#ifndef FIXED_RATIOS_ADD_HPP
#define FIXED_RATIOS_ADD_HPP

#include <fixed/operations/add.hpp>
#include <fixed/operations/div.hpp>
#include <fixed/operations/mul.hpp>
#include <fixed/ratio.hpp>
#include <fixed/ratios/simplify.hpp>

namespace fixed {
namespace detail {

template <detail::is_fixed_r R>
R transform_ratio_operation_result(const R &result, bool do_simplify)
{
	auto transformed = result;

	if (do_simplify) {
		transformed = simplify(transformed);
	}

	return transformed;
}

} // namespace detail

template <detail::is_fixed_r A, detail::is_fixed_r B>
detail::is_fixed_r auto add(const A &a, const B &b, bool do_simplify=true)
{
	const detail::is_fixed_p auto dividend =
		add(mul(a.dividend, b.divider), mul(b.dividend, a.divider));
	const detail::is_fixed_p auto divider = mul(a.divider, b.divider);

	return detail::transform_ratio_operation_result(div(dividend, divider),
		do_simplify);
}

} // namespace fixed

#endif // FIXED_RATIOS_ADD_HPP
