#ifndef FIXED_RATIOS_DIV_HPP
#define FIXED_RATIOS_DIV_HPP

#include <fixed/operations/div.hpp>
#include <fixed/operations/mul.hpp>
#include <fixed/ratio.hpp>

namespace fixed {

template <detail::is_fixed_r A, detail::is_fixed_r B>
detail::is_fixed_r auto div(const A &a, const B &b, bool do_simplify=true)
{
	const detail::is_fixed_p auto dividend = mul(a.dividend, b.divider);
	const detail::is_fixed_p auto divider = mul(a.divider, b.dividend);

	return detail::transform_ratio_operation_result(div(dividend, divider),
		do_simplify);
}

} // namespace fixed

#endif // FIXED_RATIOS_DIV_HPP
