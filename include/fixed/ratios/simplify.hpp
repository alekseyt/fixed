#ifndef FIXED_RATIOS_SIMPLIFY_HPP
#define FIXED_RATIOS_SIMPLIFY_HPP

#include <algorithm>
#include <concepts>
#include <numeric>
#include <fixed/detail/utils.hpp>
#include <fixed/exceptions.hpp>
#include <fixed/make.hpp>
#include <fixed/operations/div.hpp>
#include <fixed/ratio.hpp>

namespace fixed {
namespace detail {

template <detail::is_fixed_p A, detail::is_fixed_p B>
detail::is_fixed_p auto gcd(const A &a, const B &b)
{
	constexpr detail::asserted_precision_t max_precision =
		std::max(A::precision_v, B::precision_v);

	const detail::is_fixed_p auto a_upscaled =
		upscale<max_precision, typename A::storage_t>(a);
	const detail::is_fixed_p auto b_upscaled =
		upscale<max_precision, typename B::storage_t>(b);

	const auto gcd_v =
		std::gcd(detail::get(a_upscaled), detail::get(b_upscaled));
	// gcd should always be positive
	// common type for uint64_t and int64_t is uint64_t
	// this should fit gcd value unless it's negative
	if (gcd_v < 0) {
		throw domain_error("negative gcd");
	}

	point<max_precision, std::remove_cvref_t<decltype(gcd_v)>> result;
	const detail::asserted_precision_t accuracy =
		std::min(fixed::accuracy(a_upscaled), fixed::accuracy(b_upscaled));
	const bool is_accurate =
		(fixed::is_accurate(a_upscaled) && fixed::is_accurate(b_upscaled));
	detail::set(result, gcd_v, accuracy, is_accurate);

	return result;
}

} // namespace detail

template <detail::is_fixed_r R>
	requires (std::integral<typename R::dividend_t::storage_t>
	&& std::integral<typename R::divider_t::storage_t>)
detail::is_fixed_r auto simplify(const R &r)
{
	const detail::is_fixed_p auto gcd = detail::gcd(r.dividend, r.divider);

	const bool can_simplify = (detail::get(gcd) != 0
		&& gcd != fixed::make::p<0>(1, 0)); // no point in simplifying when gdc == 1
	if (!can_simplify) {
		return r;
	}

	const detail::is_fixed_r auto dividend_ratio = div(r.dividend, gcd);
	const detail::is_fixed_r auto divider_ratio = div(r.divider, gcd);
	// extract simplified fixed::p-s with precision matching origin values
	const detail::is_fixed_p auto dividend_simplified =
		fixed::get<R::dividend_t::precision_v>(dividend_ratio);
	const detail::is_fixed_p auto divider_simplified =
		fixed::get<R::divider_t::precision_v>(divider_ratio);

	const auto simplified = ratio(dividend_simplified, divider_simplified);

	return simplified;
}

} // namespace fixed

#endif // FIXED_RATIOS_SIMPLIFY_HPP
