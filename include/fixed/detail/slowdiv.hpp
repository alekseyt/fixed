#ifndef FIXED_DETAIL_SLOWDIV_HPP
#define FIXED_DETAIL_SLOWDIV_HPP

#include <algorithm>
#include <cassert>
#include <optional>
#include <type_traits>
#include <tuple>
#include <utility>
#include <fixed/detail/concepts.hpp>
#include <fixed/detail/overflow.hpp>
#include <fixed/make.hpp>
#include <fixed/point.hpp>
#include <fixed/utils.hpp>

namespace fixed { namespace detail {

// TODO: generator. or range? or generator? decisions, decisions...
template <typename ResultT, typename Dividend, typename Divider>
struct div_range {
	constexpr explicit div_range(Dividend dividend, Divider divider)
	: dividend(dividend)
	, divider(divider)
	{}

	ResultT yield() {
		assert(dividend >= 0);
		assert(divider > 0);

		counter = 0;
		while (dividend >= divider) {
			dividend -= divider;
			++counter;
		}
		// prepare dividend for next iteration
		// multiply by 10 so on next step subtraction could begin right away
		dividend *= 10;
		return counter;
	}

	bool completed() const {
		return (dividend == 0);
	}

private:
	Dividend dividend = 0;
	Divider divider = 1;
	// normally counter isn't going to be larger than dividend or divider,
	// it's going to be equal to dividend when divider equals 1 (one)
	// so perhaps good storage type for counter is dividend's Storage
	ResultT counter = 0;
};

template <
	typename ScaleT,
	detail::asserted_precision_t PrecisionShift,
	detail::asserted_precision_t MaxPrecision,
	detail::asserted_precision_t CurrentPrecision=(MaxPrecision-(PrecisionShift+1))
	>
	requires (detail::can_scale_to_precision<ScaleT, CurrentPrecision>())
constexpr void get_scales(std::array<ScaleT, MaxPrecision> &array)
{
	array[PrecisionShift] = detail::precision_to_scale<ScaleT, CurrentPrecision>();
	if constexpr (CurrentPrecision > 0) {
		get_scales<ScaleT, PrecisionShift+1, MaxPrecision>(array);
	}
}

template <typename ScaleT, detail::asserted_precision_t MaxPrecision>
	requires (MaxPrecision > 0
	&& detail::can_scale_to_precision<ScaleT, MaxPrecision>())
constexpr auto get_scales()
{
	std::array<ScaleT, MaxPrecision> array;
	get_scales<ScaleT, 0, MaxPrecision>(array);
	return array;
}

template <
	detail::asserted_precision_t Precision,
	detail::is_fixed_p Dividend,
	detail::is_fixed_p Divider,
	typename ResultStorageT=std::remove_cv_t<detail::common_storage_t<Dividend, Divider>>
	>
	requires (detail::can_scale_to_precision<ResultStorageT, Precision>())
detail::is_fixed_p auto slowdiv(const Dividend &dividend, const Divider &divider)
{
	using dividend_storage_t = typename Dividend::storage_t;
	using divider_storage_t = typename Divider::storage_t;

	const auto make_result =
	[] (const ResultStorageT &integral, detail::assumed_fractional_storage_t fractional,
		detail::asserted_precision_t accuracy, bool accurate) {
		return detail::p<Precision, ResultStorageT>(integral, fractional, accuracy, accurate);
	};

	constexpr detail::asserted_precision_t max_precision =
		std::max(Dividend::precision_v, Divider::precision_v);

	std::integral auto dividend_v = detail::get(dividend);
	std::integral auto divider_v = detail::get(divider);

	if (divider_v == 0) {
		throw domain_error("division by zero");
	}

	const bool divider_negative = (divider_v < 0);
	const bool need_to_change_sign = divider_negative;
	if (divider_negative) {
		divider_v = -divider_v;
	}

	if (need_to_change_sign && std::unsigned_integral<ResultStorageT>) {
		throw overflow_error("can't invert result type"); // won't be able to invert unsigned result
	}

	const bool dividend_negative = (dividend_v < 0);
	const bool need_to_invert_result = dividend_negative;
	if (dividend_negative) {
		dividend_v = -dividend_v;
	}

	// at this point both dividend and divider should be positive numbers
	if (dividend_v < 0) {
		throw out_of_range("invalid divident");
	}
	if (divider_v <= 0) {
		throw out_of_range("invalid divider"); // dbz should be handled before
	}

	const dividend_storage_t dividend_upscaled = // XXX: preserve storage
		detail::upscale<Dividend::precision_v, max_precision>(dividend_v);
	const divider_storage_t divider_upscaled = // XXX: preserve storage
		detail::upscale<Divider::precision_v, max_precision>(divider_v);

	div_range<ResultStorageT, ResultStorageT, ResultStorageT> // use same storage for both divident and divider during division
		range(dividend_upscaled, divider_upscaled); // dividend and divider converted to result storage, no static_cast

	ResultStorageT integral = range.yield(); // integral part appears at first division iteration

	// changesign | invertresult | actuallyinvert | action
	// -----------+--------------+----------------+-------
	//    false   |    false     |     false      | no action required
	//    false   |    true      |     true       | invert divider back to negative number
	//    true    |    false     |     true       | change sign as required
	//    true    |    true      |     false      | when both flags set, result don't need to be inverted, it already is
	//
	// it's a XOR
	const bool actually_invert_result = (need_to_change_sign ^ need_to_invert_result);
	if (actually_invert_result) {
		integral = -integral;
	}

	detail::assumed_fractional_storage_t fractional = 0;
	if constexpr (Precision > 0) {
		// get scales at compile time so runtime precision_to_scale() isn't required
		constexpr std::array<detail::assumed_fractional_storage_t, Precision> scales {
			get_scales<detail::assumed_fractional_storage_t, Precision>()
		};
		for (detail::asserted_precision_t i = 0; i < Precision; ++i) {
			const ResultStorageT v = range.yield();

			// when calculating fractional part, each digit should normally be in 0..9 range
			if (v < 0 || v > 9) {
				throw out_of_range("fractional digit is outside of expected range");
			}

			const std::optional<detail::assumed_fractional_storage_t> v_scaled =
				detail::check_mul_overflow<detail::assumed_fractional_storage_t>(scales[i], v);
			if (!v_scaled) {
				throw overflow_error("scale overflow in division");
			}

			const std::optional<detail::assumed_fractional_storage_t> new_fractional =
				detail::check_add_overflow<detail::assumed_fractional_storage_t>(fractional, *v_scaled);
			if (!new_fractional) {
				throw overflow_error("fraction overflow in division");
			}

			fractional = *new_fractional;
		}
	}

	using result_t = decltype(make_result(
		std::declval<ResultStorageT>(),
		detail::assumed_fractional_storage_t{},
		detail::asserted_precision_t{},
		bool{}));

	result_t result;

	const bool accurate = (is_accurate(dividend) && is_accurate(divider) && range.completed());
	const detail::asserted_precision_t effective_accuracy =
		(accurate ? result_t::precision_v : std::min(Precision, std::min(accuracy(dividend), accuracy(divider))));
	result = make_result(integral, fractional, effective_accuracy, accurate);

	return result;
}

}} // namespace detail // namespace fixed

#endif // FIXED_DETAIL_SLOWDIV_HPP
