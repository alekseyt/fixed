#ifndef FIXED_DETAIL_F_HPP
#define FIXED_DETAIL_F_HPP

#include <cstdint>

namespace fixed { namespace detail {

using assumed_fractional_storage_t = uint64_t; //< type used to store fractional

}} // namespace detail // namespace fixed

#endif // FIXED_DETAIL_F_HPP
