#ifndef FIXED_DETAIL_COMPARE_HPP
#define FIXED_DETAIL_COMPARE_HPP

#include <algorithm>
#include <cassert>
#include <concepts>
#include <compare>
#include <fixed/point.hpp>
#include <fixed/scaling.hpp>

namespace fixed { namespace detail {

template <typename A, typename B>
concept same_signess = detail::is_fixed_p<A>
&& detail::is_fixed_p<B>
&& ((std::signed_integral<typename A::storage_t> && std::signed_integral<typename B::storage_t>)
	|| (std::unsigned_integral<typename A::storage_t> && std::unsigned_integral<typename B::storage_t>));

template <typename A, typename B>
concept different_signess = detail::is_fixed_p<A>
&& detail::is_fixed_p<B>
&& ((std::signed_integral<typename A::storage_t> && std::unsigned_integral<typename B::storage_t>)
	|| (std::unsigned_integral<typename A::storage_t> && std::signed_integral<typename B::storage_t>));

template <detail::is_fixed_p A, detail::is_fixed_p B>
	requires (A::precision_v == B::precision_v)
std::partial_ordering do_compare(const A &a, const B &b)
{
	if (detail::get(a) < detail::get(b)) {
		return std::partial_ordering::less;
	}
	else if (detail::get(a) > detail::get(b)) {
		return std::partial_ordering::greater;
	}
	else if (detail::get(a) == detail::get(b)
	&& is_accurate(a)
	&& is_accurate(b)) {
		return std::partial_ordering::equivalent;
	}
	return std::partial_ordering::unordered;
}

template <detail::is_fixed_p A, detail::is_fixed_p B>
constexpr std::partial_ordering upscale_and_compare(const A &a, const B &b)
{
	using common_storage_t = detail::common_storage_t<A, B>;

	constexpr detail::asserted_precision_t max_precision =
		std::max(A::precision_v, B::precision_v);

	const detail::is_fixed_p auto a_upscaled =
		upscale<max_precision, common_storage_t>(a);
	const detail::is_fixed_p auto b_upscaled =
		upscale<max_precision, common_storage_t>(b);

	return detail::do_compare(a_upscaled, b_upscaled);
}

template <detail::is_fixed_p A,	detail::is_fixed_p B>
	requires (detail::same_signess<A, B>)
constexpr std::partial_ordering compare_raw(const A &a, const B &b)
{
	// when both operands has same signness, compare is pretty straightforward:
	// upscale both to max precision and compare raw values
	// except storage has to be "upscaled" too to "max" storage, i.e. common storage type
	return detail::upscale_and_compare(a, b);
}

template <detail::is_fixed_p A, detail::is_fixed_p B>
	requires (detail::different_signess<A, B>)
constexpr std::partial_ordering compare_raw(const A &a, const B &b)
{
	// when operands of the different signess, compare is a bit more complicated:
	// if one operand is negative and another isn't - negative one is smaller
	// otherwise compare operands as numbers with same signess
	// since sign is need to be checked at runtime, this comparison is also runtime
	if (detail::get(a) < 0 && detail::get(b) >= 0) {
		return std::partial_ordering::less;
	}
	else if (detail::get(b) < 0 && detail::get(a) >= 0) {
		return std::partial_ordering::greater;
	}
	// now compare operands as numbers with same signess
	return detail::upscale_and_compare(a, b);
}

template <detail::is_fixed_p A, detail::is_fixed_p B>
std::partial_ordering compare(const A &a, const B &b)
{
	return detail::compare_raw(a, b);
}

}} // namespace detail // namespace fixed

#endif // FIXED_DETAIL_COMPARE_HPP
