#ifndef FIXED_DETAIL_SCALING_HPP
#define FIXED_DETAIL_SCALING_HPP

#include <cassert>
#include <concepts>
#include <cmath>
#include <optional>
#include <fixed/detail/overflow.hpp>
#include <fixed/detail/p.hpp>
#include <fixed/exceptions.hpp>
#include <fixed/limits.hpp>

namespace fixed { namespace detail {

template <std::integral ScaleT, detail::asserted_precision_t Precision>
	requires (Precision >= 0)
consteval bool can_scale_to_precision()
{
	if constexpr (Precision <= limits::precision<ScaleT>::max()) {
		return true;
	}
	else {
		return false;
	}
}

template <std::integral ScaleT, detail::asserted_precision_t Precision>
	requires (Precision >= 0
	&& can_scale_to_precision<ScaleT, Precision>())
consteval ScaleT precision_to_scale()
{
	if constexpr (Precision == 0) {
		return 1;
	}
	else {
		constexpr ScaleT lower = precision_to_scale<ScaleT, Precision-1>();
		return (lower * 10);
	}
}

template <typename ScaleT, detail::asserted_precision_t FromPrecision, detail::asserted_precision_t ToPrecision>
concept can_upscale = (FromPrecision <= ToPrecision)
&& can_scale_to_precision<ScaleT, FromPrecision>()
&& can_scale_to_precision<ScaleT, ToPrecision>();

template <typename ScaleT, detail::asserted_precision_t FromPrecision, detail::asserted_precision_t ToPrecision>
concept can_downscale = (FromPrecision >= ToPrecision)
&& can_scale_to_precision<ScaleT, FromPrecision>()
&& can_scale_to_precision<ScaleT, ToPrecision>();

template <std::integral ScaleT>
constexpr bool runtime_can_scale_to_precision(detail::asserted_precision_t precision) noexcept
{
	return (precision <= limits::precision<ScaleT>::max());
}

template <std::integral ScaleT>
constexpr ScaleT runtime_precision_to_scale(detail::asserted_precision_t precision) noexcept
{
	assert(runtime_can_scale_to_precision<ScaleT>(precision));
	return std::pow(10, precision);
}

template <typename Storage>
constexpr Storage upscale(detail::asserted_precision_t from_precision,
	detail::asserted_precision_t to_precision, Storage value)
{
	if (to_precision < from_precision) {
		throw invalid_argument("can't upscale to lower precision");
	}
	else if (to_precision == from_precision) {
		return value;
	}

	if (!runtime_can_scale_to_precision<Storage>(from_precision)
	|| !runtime_can_scale_to_precision<Storage>(to_precision)) {
		throw overflow_error("can't scale to required precision");
	}

	const Storage from_scale = detail::runtime_precision_to_scale<Storage>(from_precision);
	const Storage to_scale = detail::runtime_precision_to_scale<Storage>(to_precision);
	const Storage factor = to_scale / from_scale;

	if (factor <= 0) {
		throw out_of_range("invalid scale factor");
	}

	const std::optional<Storage> opt =
		detail::check_mul_overflow<Storage>(value, factor);
	if (!opt) {
		throw overflow_error("mul overflow in upscale");
	}
	return *opt;
}

template <
	detail::asserted_precision_t FromPrecision,
	detail::asserted_precision_t ToPrecision,
	typename Storage
	>
	requires (detail::can_upscale<Storage, FromPrecision, ToPrecision>)
constexpr Storage upscale(Storage value)
{
	return upscale(FromPrecision, ToPrecision, value);
}

template <typename Storage>
constexpr Storage trim(bool &loseless, detail::asserted_precision_t from_precision,
	detail::asserted_precision_t to_precision, Storage value)
{
	if (to_precision > from_precision) {
		throw invalid_argument("can't trim to higher precision");
	}
	else if (to_precision == from_precision) {
		return value;
	}

	if (!runtime_can_scale_to_precision<Storage>(from_precision)
	|| !runtime_can_scale_to_precision<Storage>(to_precision)) {
		throw overflow_error("can't scale to required precision");
	}

	const Storage from_scale = detail::runtime_precision_to_scale<Storage>(from_precision);
	const Storage to_scale = detail::runtime_precision_to_scale<Storage>(to_precision);
	const Storage factor = from_scale / to_scale;

	if (factor <= 0) {
		throw out_of_range("invalid scale factor");
	}

	loseless = ((value % factor) == 0);

	const Storage trimmed = (value / factor);
	return trimmed;
}

template <
	detail::asserted_precision_t FromPrecision,
	detail::asserted_precision_t ToPrecision,
	typename Storage
	>
	requires (detail::can_downscale<Storage, FromPrecision, ToPrecision>)
constexpr Storage trim(bool &loseless, Storage value)
{
	return trim(loseless, FromPrecision, ToPrecision, value);
}

}} // namespace detail // namespace fixed

#endif // FIXED_DETAIL_SCALING_HPP
