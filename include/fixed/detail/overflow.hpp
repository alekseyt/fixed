#ifndef FIXED_DETAIL_OVERFLOW_HPP
#define FIXED_DETAIL_OVERFLOW_HPP

#include <concepts>
#include <optional>

// https://gcc.gnu.org/onlinedocs/gcc/Integer-Overflow-Builtins.html

namespace fixed { namespace detail {

template <std::integral T>
static constexpr std::optional<T> empty_if_overflow(bool overflow, const T &res)
{
	std::optional<T> result;
	if (!overflow) {
		result = res;
	}
	return result;
}

template <std::integral Result, std::integral A, std::integral B>
constexpr std::optional<Result> check_mul_overflow(A a, B b)
{
	Result res;
	return empty_if_overflow(__builtin_mul_overflow(a, b, &res), res);
}

template <std::integral Result, std::integral A, std::integral B>
constexpr std::optional<Result> check_add_overflow(A a, B b)
{
	Result res;
	return empty_if_overflow(__builtin_add_overflow(a, b, &res), res);
}

template <std::integral Result, std::integral A, std::integral B>
constexpr std::optional<Result> check_sub_overflow(A a, B b)
{
	Result res;
	return empty_if_overflow(__builtin_sub_overflow(a, b, &res), res);
}

}} // namespace detail // namespace fixed

#endif // FIXED_DETAIL_OVERFLOW_HPP
