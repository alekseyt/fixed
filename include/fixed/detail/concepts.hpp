#ifndef FIXED_DETAIL_CONCEPTS_HPP
#define FIXED_DETAIL_CONCEPTS_HPP

#include <concepts>
#include <optional>
#include <type_traits>

namespace fixed { namespace detail {

template <typename T>
concept is_optional = requires (T t) {
	{ std::optional{t} } -> std::same_as<std::remove_cvref_t<T>>;
};

}} // namespace detail // namespace fixed

#endif // FIXED_DETAIL_CONCEPTS_HPP
