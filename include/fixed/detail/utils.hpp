#ifndef FIXED_DETAIL_UTILS_HPP
#define FIXED_DETAIL_UTILS_HPP

#include <fixed/detail/f.hpp>

namespace fixed { namespace detail {

constexpr detail::asserted_precision_t fraction_order(detail::assumed_fractional_storage_t fraction) noexcept
{
	return (std::floor(std::log10(fraction)) + 1);
}

}} // namespace detail // namespace fixed

#endif // FIXED_DETAIL_UTILS_HPP
