#ifndef FIXED_DECOMPOSE_HPP
#define FIXED_DECOMPOSE_HPP

#include <fixed/fraction.hpp>
#include <fixed/point.hpp>
#include <fixed/utils.hpp>

namespace fixed { namespace detail {

template <detail::is_fixed_p P>
constexpr typename P::storage_t integer(const P &p) noexcept
{
	const typename P::storage_t integer = detail::get(p) / detail::scale(p);
	return integer;
}

template <detail::is_fixed_p P>
constexpr detail::is_fixed_f auto fractional(const P &p) noexcept
{
	fixed::f<P::precision_v> f;

	const std::integral auto raw = detail::get(p);
	const std::integral auto integer_scaled = integer(p) * P::scale_v;
	const std::integral auto fract = (raw - integer_scaled);
	detail::set(f, (fract < 0 ? -fract : fract), P::precision_v, is_accurate(p));

	return f;
}

}} // namespace detail // namespace fixed

#endif // FIXED_DECOMPOSE_HPP
