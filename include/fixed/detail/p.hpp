#ifndef FIXED_DETAIL_P_HPP
#define FIXED_DETAIL_P_HPP

#include <cstdint>

namespace fixed { namespace detail {

using asserted_precision_t = uint8_t; // type used to store p's precision
using assumed_integer_storage_t = int64_t; //< default storage for p

}} // namespace detail // namespace fixed

#endif // FIXED_DETAIL_P_HPP
