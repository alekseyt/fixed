#ifndef FIXED_FRACTION_HPP
#define FIXED_FRACTION_HPP

#include <type_traits>
#include <fixed/detail/concepts.hpp>
#include <fixed/detail/f.hpp>
#include <fixed/point.hpp>

namespace fixed {

template <detail::asserted_precision_t Precision>
struct fraction : p<Precision, detail::assumed_fractional_storage_t> {};

template <detail::asserted_precision_t Precision>
using f = fraction<Precision>;

namespace detail {

template <typename T>
concept is_fixed_f = requires (T t) {
	{ fixed::fraction{t} } -> std::same_as<std::remove_cvref_t<T>>;
};

template <typename T>
concept is_optional_f = is_optional<T>
&& detail::is_fixed_f<typename std::remove_cvref_t<T>::value_type>;

} // namespace detail

namespace detail {

template <detail::is_fixed_f F>
constexpr const typename F::storage_t& get(const F &f) noexcept
{
	return f.value;
}

template <detail::is_fixed_f F, typename V=typename F::storage_t>
constexpr void set(F &f, const V &v, detail::asserted_precision_t accuracy, bool accurate)
{
	f.value = v;
	f.accuracy = accuracy;  // FIXME: should it really accept accuracy when precision is always 0?
	f.accurate = accurate;
}

} // namespace detail

} // namespace fixed

#endif // FIXED_FRACTION_HPP
