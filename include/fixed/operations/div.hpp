#ifndef FIXED_OPERATIONS_DIV_HPP
#define FIXED_OPERATIONS_DIV_HPP

#include <fixed/point.hpp>
#include <fixed/ratio.hpp>

namespace fixed {

template <detail::is_fixed_p A, detail::is_fixed_p B>
detail::is_fixed_r auto div(const A &a, const B &b)
{
	const detail::is_fixed_r auto result = fixed::ratio(a, b);
	return result;
}

} // namespace fixed

#endif // FIXED_OPERATIONS_DIV_HPP
