#ifndef FIXED_OPERATIONS_SUB_HPP
#define FIXED_OPERATIONS_SUB_HPP

#include <algorithm>
#include <optional>
#include <fixed/detail/concepts.hpp>
#include <fixed/point.hpp>
#include <fixed/utils.hpp>

namespace fixed {

template <
	detail::is_fixed_p A,
	detail::is_fixed_p B,
	typename ResultStorageT=std::remove_cv_t<detail::common_storage_t<A, B>>,
	detail::asserted_precision_t TargetPrecision=std::max(A::precision_v, B::precision_v)
	>
	requires (detail::can_scale_to_precision<ResultStorageT, TargetPrecision>())
detail::is_fixed_p auto sub(const A &a, const B &b)
{
	const typename A::storage_t scaled_a = // XXX: preserve storage type
		detail::upscale<A::precision_v, TargetPrecision>(detail::get(a));
	const typename B::storage_t scaled_b = // XXX: preserve storage type
		detail::upscale<B::precision_v, TargetPrecision>(detail::get(b));

	const std::optional<ResultStorageT> opt =
		detail::check_sub_overflow<ResultStorageT>(scaled_a, scaled_b);
	if (!opt) {
		throw overflow_error("sub underflow");
	}

	using p_t = p<TargetPrecision, ResultStorageT>;

	p_t result;

	const bool accurate = (is_accurate(a) && is_accurate(b));
	const detail::asserted_precision_t effective_accuracy =
		(accurate ? p_t::precision_v : std::min(accuracy(a), accuracy(b)));
	detail::set(result, *opt, effective_accuracy, accurate);

	return result;
}

} // namespace fixed

#endif // FIXED_OPERATIONS_SUB_HPP
