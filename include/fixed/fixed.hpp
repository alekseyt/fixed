#ifndef FIXED_FIXED_HPP
#define FIXED_FIXED_HPP

#include <fixed/exceptions.hpp>
#include <fixed/fraction.hpp>
#include <fixed/make.hpp>
#include <fixed/operators.hpp>
#include <fixed/operations/add.hpp>
#include <fixed/operations/div.hpp>
#include <fixed/operations/mul.hpp>
#include <fixed/operations/sub.hpp>
#include <fixed/point.hpp>
#include <fixed/ratio.hpp>
#include <fixed/ratios/add.hpp>
#include <fixed/ratios/div.hpp>
#include <fixed/ratios/mul.hpp>
#include <fixed/ratios/simplify.hpp>
#include <fixed/ratios/sub.hpp>
#include <fixed/roperators.hpp>
#include <fixed/scaling.hpp>
#include <fixed/tostring.hpp>
#include <fixed/utils.hpp>

#endif // FIXED_FIXED_HPP
