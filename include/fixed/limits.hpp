#ifndef FIXED_LIMITS_HPP
#define FIXED_LIMITS_HPP

#include <algorithm>
#include <concepts>
#include <limits>
#include <fixed/detail/p.hpp>

namespace fixed { namespace limits {
namespace detail {

template <std::integral StorageT, StorageT storage_max, StorageT exponent=1>
consteval StorageT max_exp10()
{
	if constexpr (storage_max == 0) {
		return 0;
	}
	else if constexpr (storage_max <= 9) {
		return exponent;
	}
	else {
		return max_exp10<StorageT, (storage_max/10), (exponent*10)>();
	}
}

template <std::integral StorageT, StorageT exp, StorageT order=1>
consteval StorageT exp10_order()
{
	if constexpr (exp <= 1) {
		return 0;
	}
	else {
		return (1 + exp10_order<StorageT, (exp/10), (order+1)>());
	}
}

template <std::integral ScaleT>
consteval ScaleT scale_max()
{
	constexpr ScaleT storage_max = std::numeric_limits<ScaleT>::max();
	constexpr ScaleT exp10_max = detail::max_exp10<ScaleT, storage_max>();
	return exp10_max;
}

} // namespace detail

template <typename ScaleT, std::unsigned_integral PrecisionT=fixed::detail::asserted_precision_t>
struct precision {

static consteval std::unsigned_integral auto max()
{
	// std::numeric_limits<PrecisionT>::max() is theoretical limit of precision
	// however there is also scale that has its own limit
	// therefore practical limit of PrecisionT is min(limit(scale), limit(precision))
	constexpr PrecisionT precision_limit = std::numeric_limits<PrecisionT>::max();
	constexpr ScaleT max_scale_exp10 = detail::scale_max<ScaleT>();
	constexpr ScaleT scale_limit = detail::exp10_order<ScaleT, max_scale_exp10>();
	static_assert(scale_limit <= precision_limit);
	return std::min(static_cast<PrecisionT>(scale_limit), precision_limit);
}

};

}} // namespace limits // namespace fixed

#endif // FIXED_LIMITS_HPP
