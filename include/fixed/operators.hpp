#ifndef FIXED_OPERATORS_HPP
#define FIXED_OPERATORS_HPP

#include <compare>
#include <fixed/detail/compare.hpp>
#include <fixed/operations/add.hpp>
#include <fixed/operations/div.hpp>
#include <fixed/operations/mul.hpp>
#include <fixed/operations/sub.hpp>
#include <fixed/point.hpp>

namespace fixed {

template <detail::is_fixed_p A, detail::is_fixed_p B>
detail::is_fixed_p auto operator+ (const A &a, const B &b)
{
	return add(a, b);
}

template <detail::is_fixed_p A, detail::is_fixed_p B>
detail::is_fixed_p auto operator- (const A &a, const B &b)
{
	return sub(a, b);
}

template <detail::is_fixed_p A, detail::is_fixed_p B>
detail::is_fixed_p auto operator* (const A &a, const B &b)
{
	return mul(a, b);
}

template <detail::is_fixed_p A, detail::is_fixed_p B>
detail::is_fixed_r auto operator/ (const A &a, const B &b)
{
	return div(a, b);
}

template <detail::is_fixed_p A, detail::is_fixed_p B>
std::partial_ordering operator<=> (const A &a, const B &b)
{
	return detail::compare(a, b);
}

template <detail::is_fixed_p A, detail::is_fixed_p B>
bool operator== (const A &a, const B &b)
{
	return (operator<=>(a, b) == std::partial_ordering::equivalent);
}

} // namespace fixed

#endif // FIXED_OPERATORS_HPP
