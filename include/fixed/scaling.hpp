#ifndef FIXED_SCALING_HPP
#define FIXED_SCALING_HPP

#include <algorithm>
#include <fixed/detail/p.hpp>

namespace fixed {

template <detail::asserted_precision_t ToPrecision, typename Storage, detail::is_fixed_p P>
	requires (detail::can_upscale<Storage, P::precision_v, ToPrecision>)
constexpr detail::is_fixed_p auto upscale(const P &p)
{
	const Storage p_upscaled =
		detail::upscale<P::precision_v, ToPrecision, Storage>(detail::get(p));

	fixed::p<ToPrecision, Storage> result;

	const bool accurate = is_accurate(p);
	const detail::asserted_precision_t effective_accuracy =
		(accurate ? ToPrecision : std::min(ToPrecision, accuracy(p)));
	detail::set(result, p_upscaled, effective_accuracy, accurate);
	return result;
}

template <detail::asserted_precision_t ToPrecision, detail::is_fixed_p P>
	requires (detail::can_upscale<typename P::storage_t, P::precision_v, ToPrecision>)
constexpr detail::is_fixed_p auto upscale(const P &p)
{
	return upscale<ToPrecision, typename P::storage_t>(p);
}

template <detail::asserted_precision_t ToPrecision, typename Storage, detail::is_fixed_p P>
	requires (detail::can_downscale<typename P::storage_t, P::precision_v, ToPrecision>)
constexpr detail::is_fixed_p auto downscale(const P &p)
{
	bool loseless_trim = false;
	const Storage p_downscaled =
		detail::trim<P::precision_v, ToPrecision>(loseless_trim, detail::get(p));

	fixed::p<ToPrecision, Storage> result;

	const bool accurate = (is_accurate(p) && loseless_trim);
	const detail::asserted_precision_t effective_accuracy =
		(accurate ? ToPrecision : std::min(ToPrecision, accuracy(p)));
	detail::set(result, p_downscaled, effective_accuracy, accurate);
	return result;
}

template <detail::asserted_precision_t ToPrecision, detail::is_fixed_p P>
	requires (detail::can_downscale<typename P::storage_t, P::precision_v, ToPrecision>)
constexpr detail::is_fixed_p auto downscale(const P &p)
{
	return downscale<ToPrecision, typename P::storage_t>(p);
}

} // namespace fixed

#endif // FIXED_SCALING_HPP
