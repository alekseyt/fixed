#ifndef FIXED_EXCEPTIONS_HPP
#define FIXED_EXCEPTIONS_HPP

#include <concepts>
#include <stdexcept>
#include <type_traits>

namespace fixed {

using exception = std::exception;

using overflow_error = std::overflow_error;
static_assert(std::is_base_of_v<exception, overflow_error>);

using domain_error = std::domain_error;
static_assert(std::is_base_of_v<exception, domain_error>);

using out_of_range = std::out_of_range;
static_assert(std::is_base_of_v<exception, out_of_range>);

using invalid_argument = std::invalid_argument;
static_assert(std::is_base_of_v<exception, invalid_argument>);

} // namespace fixed

#endif // FIXED_EXCEPTIONS_HPP
