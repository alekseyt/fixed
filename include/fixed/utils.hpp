#ifndef FIXED_UTILS_HPP
#define FIXED_UTILS_HPP

#include <type_traits>
#include <fixed/detail/concepts.hpp>
#include <fixed/detail/utils.hpp>
#include <fixed/point.hpp>

namespace fixed {

template <detail::is_fixed_p P>
constexpr bool is_accurate(const P &p) noexcept
{
	return (p.accurate && p.accuracy >= P::precision_v);
}

template <detail::is_fixed_p P>
consteval auto precision() noexcept
{
	using clean_t = std::remove_cvref_t<P>;
	return clean_t::precision_v;
}

template <detail::is_fixed_p P>
constexpr typename P::precision_t precision(const P &) noexcept
{
	return precision<P>();
}

template <detail::is_fixed_p P>
constexpr typename P::precision_t accuracy(const P &p) noexcept
{
	return p.accuracy;
}

namespace detail {

template <detail::is_fixed_f F>
constexpr bool is_accurate(const F &f) noexcept
{
	return (f.accurate && f.accuracy >= F::precision_v);
}

template <detail::is_fixed_f F>
constexpr typename F::precision_t accuracy(const F &f) noexcept
{
	return f.accuracy;
}

} // namespace detail

} // namespace fixed

#endif // FIXED_UTILS_HPP
