#ifndef FIXED_TOSTRING_HPP
#define FIXED_TOSTRING_HPP

#include <iomanip>
#include <sstream>
#include <fixed/detail/decompose.hpp>
#include <fixed/point.hpp>

namespace fixed {

template <detail::is_fixed_p P>
std::string to_string(const P &p)
{
	// TODO: std::format
	std::stringstream ss;

	const std::integral auto integer_part = detail::integer(p);
	ss << integer_part << ".";

	if constexpr (P::precision_v <= 0) {
		return ss.str();
	}

	const detail::is_fixed_f auto fract = detail::fractional(p);
	const std::integral auto fractional_part = detail::get(fract);
	ss << std::setw(precision(p)) << std::setfill('0') << fractional_part;

	const std::string str = ss.str();
	return str;
}

} // namespace fixed

#endif // FIXED_TOSTRING_HPP
