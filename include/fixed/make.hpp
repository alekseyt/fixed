#ifndef FIXED_MAKE_HPP
#define FIXED_MAKE_HPP

#include <cstdint>
#include <concepts>
#include <optional>
#include <fixed/detail/concepts.hpp>
#include <fixed/detail/utils.hpp>
#include <fixed/exceptions.hpp>
#include <fixed/fraction.hpp>
#include <fixed/point.hpp>
#include <fixed/utils.hpp>

namespace fixed {
namespace detail {

// I should be convertible to Storage
template <typename I, typename Storage>
concept i_compatible_with_storage = requires (I &&i, Storage &&s) {
	typename std::common_type_t<I, Storage>;
} && std::same_as<Storage, std::common_type_t<I, Storage>>;

template <typename I, typename Storage>
concept i_compatible_with_make = std::integral<I>
&& i_compatible_with_storage<I, Storage>;

} // namespace detail

namespace detail {

template <detail::asserted_precision_t Precision, typename Storage, typename I>
	requires (detail::i_compatible_with_make<I, Storage>
	&& detail::can_scale_to_precision<Storage, Precision>())
constexpr detail::is_fixed_p auto p(I integer, detail::assumed_fractional_storage_t fraction,
	detail::asserted_precision_t accuracy, bool accurate)
{
	using p_t = fixed::p<Precision, Storage>;

	const std::optional<Storage> integer_scaled =
		detail::check_mul_overflow<Storage>(integer, p_t::scale_v);
	if (!integer_scaled) {
		throw overflow_error("integer overflow");
	}

	// the trick here is to make fraction with the same sign as integer
	// so add actually adds fraction to integer in case when integer is negative
	// for unsigned types (integer < 0) is always false
	const Storage fraction_signed =
		static_cast<Storage>(fraction) * (integer < 0 ? -1 : 1);
	const std::optional<Storage> res =
		detail::check_add_overflow<Storage>(*integer_scaled, fraction_signed);
	if (!res) {
		throw overflow_error("fractional overflow");
	}

	p_t result;
	detail::set(result, *res, accuracy, accurate);

	return result;
}

template <detail::asserted_precision_t Precision, typename Storage>
	requires (detail::can_scale_to_precision<Storage, Precision>())
constexpr detail::is_fixed_f auto f(detail::assumed_fractional_storage_t fraction,
	detail::asserted_precision_t accuracy)
{
	const detail::asserted_precision_t fraction_order =
		(fraction == 0 ? 0 : detail::fraction_order(fraction));
	const bool need_trimming = (fraction_order > Precision);
	bool loseless_trim = false;

	if (need_trimming) {
		const Storage trimmed =
			detail::trim(loseless_trim, fraction_order, Precision, fraction);
		fraction = trimmed;
	}
	else {
		const Storage scaled =
			detail::upscale<Storage>(fraction_order, Precision, fraction);
		fraction = scaled;
	}

	fixed::f<Precision> result;

	const bool accurate = (!need_trimming || loseless_trim);
	detail::set(result, fraction, accuracy, accurate);

	return result;
}

} // namespace detail

namespace make {

template <detail::asserted_precision_t Precision, typename Storage=int64_t>
	requires (detail::can_scale_to_precision<Storage, Precision>())
constexpr detail::is_fixed_f auto f(detail::assumed_fractional_storage_t fraction)
{
	const auto result = detail::f<Precision, Storage>(fraction, Precision);
	return result;
}

// Make fixed::p + fixed::f
template <detail::asserted_precision_t Precision, typename Storage=int64_t, typename I, detail::is_fixed_f F>
	requires (detail::i_compatible_with_make<I, Storage>
	&& detail::can_scale_to_precision<Storage, Precision>())
constexpr detail::is_fixed_p auto p(I integer, F f, bool accurate=true)
{
	const auto result = detail::p<Precision, Storage>(integer, detail::get(f),
		std::min(Precision, detail::accuracy(f)), (accurate && detail::is_accurate(f)));
	return result;
}

// Make fixed::p + fixed::f(unsigned, Precision)
template <detail::asserted_precision_t Precision, typename Storage=int64_t, typename I>
	requires (detail::i_compatible_with_make<I, Storage>
	&& detail::can_scale_to_precision<Storage, Precision>())
constexpr detail::is_fixed_p auto p(I integer, detail::assumed_fractional_storage_t fraction, bool accurate=true)
{
	const auto result = make::p<Precision, Storage>(integer,
		make::f<Precision, Storage>(fraction), accurate);
	return result;
}

} // namespace make

} // namespace fixed

#endif // FIXED_MAKE_HPP
