#ifndef FIXED_ROPERATORS_HPP
#define FIXED_ROPERATORS_HPP

#include <fixed/operations/div.hpp>
#include <fixed/ratios/add.hpp>
#include <fixed/ratios/sub.hpp>

namespace fixed {

template <detail::is_fixed_r A, detail::is_fixed_r B>
detail::is_fixed_r auto operator+ (const A &a, const B &b)
{
	return radd(a, b);
}

template <detail::is_fixed_r A, detail::is_fixed_p B>
detail::is_fixed_r auto operator+ (const A &a, const B &b)
{
	return radd(a, div(b, fixed::make::p<0>(1, 0))); // a + b/1
}

template <detail::is_fixed_p A, detail::is_fixed_r B>
detail::is_fixed_r auto operator+ (const A &a, const B &b)
{
	return operator+(b, a);
}

template <detail::is_fixed_r A, detail::is_fixed_r B>
detail::is_fixed_r auto operator- (const A &a, const B &b)
{
	return rsub(a, b);
}

template <detail::is_fixed_r A, detail::is_fixed_p B>
detail::is_fixed_r auto operator- (const A &a, const B &b)
{
	return rsub(a, div(b, fixed::make::p<0>(1, 0))); // a - b/1
}

template <detail::is_fixed_p A, detail::is_fixed_r B>
detail::is_fixed_r auto operator- (const A &a, const B &b)
{
	return rsub(div(a, fixed::make::p<0>(1, 0)), b); // a/1 - b
}

} // namespace fixed

#endif // FIXED_ROPERATORS_HPP
