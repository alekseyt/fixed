#ifndef FIXED_POINT_HPP
#define FIXED_POINT_HPP

#include <cstdint>
#include <type_traits>
#include <fixed/detail/concepts.hpp>
#include <fixed/detail/p.hpp>
#include <fixed/detail/scaling.hpp>

namespace fixed {

/// fixed::p(oint)
template <detail::asserted_precision_t Precision, typename Storage=detail::assumed_integer_storage_t>
	requires (detail::can_scale_to_precision<Storage, Precision>())
struct point {
	using storage_t = Storage;
	using precision_t = decltype(Precision);

	static constexpr precision_t precision_v = Precision; //< decimal precision, 1 means 1 decimal place
	static constexpr storage_t scale_v = detail::precision_to_scale<Storage, Precision>(); //< decimal factor (exponent) at which integer has to be scaled to leave place for fraction

	explicit point() = default;

	Storage value = {};
	bool accurate = true;
	precision_t accuracy = 0;
};

template <detail::asserted_precision_t Precision, typename Storage=detail::assumed_integer_storage_t>
using p = point<Precision, Storage>;

namespace detail {

template <typename T>
concept is_fixed_p = requires (T t) {
	{ fixed::point{t} } -> std::same_as<std::remove_cvref_t<T>>;
};

template <typename T>
concept is_optional_p = is_optional<T>
&& detail::is_fixed_p<typename std::remove_cvref_t<T>::value_type>;

template <detail::is_fixed_p Fixed1, detail::is_fixed_p Fixed2>
struct common_storage {
	using type = std::common_type_t<typename Fixed1::storage_t, typename Fixed2::storage_t>;
};

template <typename Fixed1, typename Fixed2>
using common_storage_t = typename common_storage<Fixed1, Fixed2>::type;

} // namespace detail

namespace detail {

template <detail::is_fixed_p P>
constexpr const typename P::storage_t& get(const P &p) noexcept
{
	return p.value;
}

template <detail::is_fixed_p P, typename V=typename P::storage_t>
constexpr void set(P &p, const V &v, detail::asserted_precision_t accuracy, bool accurate)
{
	p.value = v;
	p.accuracy = accuracy;
	p.accurate = accurate;
}

template <detail::is_fixed_p P>
consteval auto scale() noexcept
{
	using clean_t = std::remove_cvref_t<P>;
	return clean_t::scale_v;
}

template <detail::is_fixed_p P>
constexpr auto scale(const P &) noexcept
{
	return scale<P>();
}

} // namespace detail

} // namespace fixed

#endif // FIXED_POINT_HPP
