#ifndef FIXED_RATIO_HPP
#define FIXED_RATIO_HPP

#include <fixed/detail/slowdiv.hpp>
#include <fixed/point.hpp>

namespace fixed {

template <detail::is_fixed_p Dividend, detail::is_fixed_p Divider>
struct ratio {
	using dividend_t = Dividend;
	using divider_t = Divider;

	dividend_t dividend;
	divider_t divider;

	ratio() = delete;
	explicit ratio(dividend_t dividend, divider_t divider)
	: dividend(dividend)
	, divider(divider)
	{}
};

template <detail::is_fixed_p Dividend, detail::is_fixed_p Divider>
using r = ratio<Dividend, Divider>;

namespace detail {

template <typename T>
concept is_fixed_r = requires (T t) {
	{ fixed::ratio{t} } -> std::same_as<std::remove_cvref_t<T>>;
};

template <typename T>
concept is_optional_r = is_optional<T>
&& detail::is_fixed_r<typename std::remove_cvref_t<T>::value_type>;

} // namespace detail

template <detail::asserted_precision_t Precision, detail::is_fixed_r R>
constexpr detail::is_fixed_p auto get(const R &r)
{
	return detail::slowdiv<Precision>(r.dividend, r.divider);
}

} // namespace fixed

#endif // FIXED_RATIO_HPP
