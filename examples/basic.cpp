#include <cassert>
#include <iostream>
#include <iomanip>
#include <fixed/fixed.hpp>

// TODO: std::format

int main()
{
	// create fixed-point (fixed::p) numbers
	// make::p() losely follows floating-point sematics: 1.5 -> (1, 5)
	const auto x = fixed::make::p<2, uint64_t>(1, 5);
	const auto y = fixed::make::p<3>(2, 7);

	const auto print_fixed = [](const std::string &title, const fixed::detail::is_fixed_p auto &x) {
		// fixed::p's precision is always known at compile time
		constexpr unsigned precision = fixed::precision<decltype(x)>();
		std::cout
			<< title << fixed::to_string(x)
			<< " (precision: " << precision << ")"
		<< std::endl;
	};

	print_fixed("x: ", x);
	print_fixed("y: ", y);

	{
		// add two numbers
		const auto added = x + y;
		// note that precision of result is maximum precision of operands
		static_assert(fixed::precision(added) == fixed::precision(y));
		print_fixed("added: ", added);
	}

	{
		// sub two numbers
		try {
			const auto subed1 = x - y;
			print_fixed("subed1: ", subed1);
		}
		catch (const fixed::overflow_error &) {
			// oops, it's underflow
		}
		// let's try the other way around
		const auto subed2 = y - x;
		// looks good to me
		print_fixed("subed2: ", subed2);
	}

	{
		// mul two numbers
		const auto muled = x * y;
		// what is the precision of mul result?
		print_fixed("muled: ", muled);
	}

	{
		// now the hard part: div two numbers
		const auto dived = x / y;
		// now this isn't fixed::p, it's fixed::r!
		static_assert(fixed::detail::is_fixed_r<decltype(dived)>);
		// what is the precision of fixed::e?
		// nobody knows, actually 1.5 / 2.7 = 0.555555...
		// so extract it from fixed::e with *some* finite precision
		const auto extracted = fixed::get<9>(dived);
		// now it's fixed::p!
		static_assert(fixed::detail::is_fixed_p<decltype(extracted)>);
		print_fixed("dived: ", extracted);
	}

	// let's see what is the precision limit if uint64_t is used as storage
	constexpr auto uint64_max_precision = fixed::limits::precision<uint64_t>::max();
	std::cout
		<< "uint64_t maximum precision: " << static_cast<int>(uint64_max_precision)
	<< std::endl;
	// is it different from maximum precision if int64_t is used as storage?
	constexpr auto int64_max_precision = fixed::limits::precision<int64_t>::max();
	std::cout
		<< "int64_t maximum precision: " << static_cast<int>(int64_max_precision)
	<< std::endl;

	return 0;
}
