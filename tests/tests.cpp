#include <array>
#include <iostream>
#include <functional>

void test_overflow_mul();
void test_overflow_add();
void test_overflow_sub();

void test_utils_upscale_comptime();
void test_utils_upscale_runtime();
void test_utils_upscale_overflow();
void test_utils_trim_comptime();
void test_utils_trim_runtime();
void test_utils_fraction_order();
void test_utils_precision();
void test_utils_precision_constexpr();

void test_make();
void test_make_with_f();
void test_make_accuracy();
void test_make_precision_shift();
void test_make_loseless_trim();
void test_make_negative();
void test_make_overflow_value();
void test_make_precision_max();

void test_p_assignment();
void test_p_accuracy();
void test_p_zero_precision();
void test_p_scale();
void test_p_scale_constexpr();
void test_f_assignment();
void test_f_accuracy();
void test_f_props();
void test_f_zero_precision();
void test_f_loseless_trim();
void test_f_trimming();

void test_decompose_integer();
void test_decompose_fractional();

void test_add();
void test_add_accuracy();
void test_add_overflow();
void test_add_negative();
void test_sub();
void test_sub_accuracy();
void test_sub_signed();
void test_sub_overflow();
void test_sub_negative();
void test_mul();
void test_mul_accuracy();
void test_mul_overflow();
void test_mul_negative();
void test_div();
void test_div_accuracy();
void test_div_inf();
void test_div_exact();
void test_div_dbz();
void test_div_negative();

void test_opers_plus_raws();
void test_opers_minus_raws();
void test_opers_multiply_raws();
void test_opers_divide_raws();

void test_mixing_types_add_uint64_int64_underflow();
void test_mixing_types_add_uint64_int64();
void test_mixing_types_sub_uint64_int64_underflow();
void test_mixing_types_sub_uint64_int64();
void test_mixing_types_mul_uint64_int64_underflow();
void test_mixing_types_mul_uint64_int64();
void test_mixing_types_div_uint64_int64_underflow();
void test_mixing_types_div_uint64_int64();

void test_scaling_upscale();
void test_scaling_upscale_overflow();
void test_scaling_upscale_accuracy();
void test_scaling_downscale();
void test_scaling_downscale_loseless();
void test_scaling_downscale_accuracy();
void test_scaling_precision_overflow();

void test_to_string();

void test_compare_same_signness_less_more();
void test_compare_same_signness_equal_unequal();
void test_compare_same_signness_accuracy();
void test_compare_diff_signness_less_more();
void test_compare_diff_signness_equal_unequal();
void test_compare_diff_signness_mix_types();

void test_accuracy_make_p();
void test_accuracy_downscale();
void test_accuracy_upscale();
void test_accuracy_downscale_upscale_downscale();
void test_accuracy_add();
void test_accuracy_sub();
void test_accuracy_mul();
void test_accuracy_div();

void test_readme_how_to_use();
void test_readme_compile_time_precision_checks();

void test_mul_15_by_27();
void test_div_15_by_27();

void test_ratios_add();
void test_ratios_add_same_precision();
void test_ratios_add_simplify();
void test_ratios_sub();
void test_ratios_sub_same_precision();
void test_ratios_sub_simplify();
void test_ratios_mul();
void test_ratios_mul_simplify();
void test_ratios_div();
void test_ratios_div_simplify();
void test_ratios_simplify();

int main()
{
	const std::array tests = {
		test_overflow_mul,
		test_overflow_add,
		test_overflow_sub,

		test_utils_upscale_comptime,
		test_utils_upscale_runtime,
		test_utils_upscale_overflow,
		test_utils_trim_comptime,
		test_utils_trim_runtime,
		test_utils_fraction_order,
		test_utils_precision,
		test_utils_precision_constexpr,

		test_make,
		test_make_with_f,
		test_make_accuracy,
		test_make_precision_shift,
		test_make_loseless_trim,
		test_make_negative,
		test_make_overflow_value,
		test_make_precision_max,

		test_p_assignment,
		test_p_accuracy,
		test_p_zero_precision,
		test_p_scale,
		test_p_scale_constexpr,
		test_f_assignment,
		test_f_accuracy,
		test_f_props,
		test_f_zero_precision,
		test_f_loseless_trim,
		test_f_trimming,

		test_decompose_integer,
		test_decompose_fractional,

		test_add,
		test_add_accuracy,
		test_add_overflow,
		test_add_negative,
		test_sub,
		test_sub_accuracy,
		test_sub_signed,
		test_sub_overflow,
		test_sub_negative,
		test_mul,
		test_mul_accuracy,
		test_mul_overflow,
		test_mul_negative,
		test_div,
		test_div_accuracy,
		test_div_inf,
		test_div_exact,
		test_div_dbz,
		test_div_negative,

		test_opers_plus_raws,
		test_opers_minus_raws,
		test_opers_multiply_raws,
		test_opers_divide_raws,

		test_mixing_types_add_uint64_int64_underflow,
		test_mixing_types_add_uint64_int64,
		test_mixing_types_sub_uint64_int64_underflow,
		test_mixing_types_sub_uint64_int64,
		test_mixing_types_mul_uint64_int64_underflow,
		test_mixing_types_mul_uint64_int64,
		test_mixing_types_div_uint64_int64_underflow,
		test_mixing_types_div_uint64_int64,

		test_scaling_upscale,
		test_scaling_upscale_overflow,
		test_scaling_upscale_accuracy,
		test_scaling_downscale,
		test_scaling_downscale_loseless,
		test_scaling_downscale_accuracy,
		test_scaling_precision_overflow,

		test_to_string,

		test_compare_same_signness_less_more,
		test_compare_same_signness_equal_unequal,
		test_compare_same_signness_accuracy,
		test_compare_diff_signness_less_more,
		test_compare_diff_signness_equal_unequal,
		test_compare_diff_signness_mix_types,

		test_accuracy_make_p,
		test_accuracy_downscale,
		test_accuracy_upscale,
		test_accuracy_downscale_upscale_downscale,
		test_accuracy_add,
		test_accuracy_sub,
		test_accuracy_mul,
		test_accuracy_div,

		test_readme_how_to_use,
		test_readme_compile_time_precision_checks,

		test_mul_15_by_27,
		test_div_15_by_27,

		test_ratios_add,
		test_ratios_add_same_precision,
		test_ratios_add_simplify,
		test_ratios_sub,
		test_ratios_sub_same_precision,
		test_ratios_sub_simplify,
		test_ratios_mul,
		test_ratios_mul_simplify,
		test_ratios_div,
		test_ratios_div_simplify,
		test_ratios_simplify,
	};

	for (const auto &test : tests) {
		std::invoke(test);
		std::cout << ".";
	}
	// TODO: std::format()
	std::cout << "\n" << std::size(tests) << " test(s) passed" << std::endl;

	return 0;
}
