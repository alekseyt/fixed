#include <cstdint>
#include <optional>
#include <fixed/fixed.hpp>

// static checks
static_assert(fixed::detail::is_fixed_p<fixed::p<2>>);
static_assert(fixed::detail::is_fixed_p<const fixed::p<2>>);
static_assert(fixed::detail::is_fixed_p<const fixed::p<2> &>);

static_assert(fixed::detail::is_optional_p<std::optional<fixed::p<2>>>);
static_assert(fixed::detail::is_optional_p<const std::optional<const fixed::p<2>>>);

static_assert(fixed::detail::is_fixed_f<fixed::f<2>>);
static_assert(fixed::detail::is_fixed_f<const fixed::f<2>>);
static_assert(fixed::detail::is_fixed_f<const fixed::f<2> &>);

static_assert(fixed::detail::is_optional_f<std::optional<fixed::f<2>>>);
static_assert(fixed::detail::is_optional_f<const std::optional<const fixed::f<2>>>);

static_assert(fixed::detail::is_fixed_r<fixed::r<fixed::p<2>, fixed::p<2>>>);
static_assert(fixed::detail::is_fixed_r<const fixed::r<fixed::p<2>, fixed::p<2>>>);
static_assert(fixed::detail::is_fixed_r<const fixed::r<fixed::p<2>, fixed::p<2>> &>);

static_assert(fixed::detail::is_optional_r<std::optional<fixed::r<fixed::p<2>, fixed::p<2>>>>);
static_assert(fixed::detail::is_optional_r<const std::optional<const fixed::r<fixed::p<2>, fixed::p<2>>>>);
