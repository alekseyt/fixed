#include <cassert>
#include <fixed/fixed.hpp>

void test_accuracy_make_p()
{
	assert(fixed::accuracy(fixed::make::p<3>(0, 111)) == 3);
	assert(fixed::accuracy(fixed::make::p<2>(0, 111)) == 2);
	assert(fixed::accuracy(fixed::make::p<1>(0, 111)) == 1);
	assert(fixed::accuracy(fixed::make::p<0>(0, 111)) == 0);
}

void test_accuracy_downscale()
{
	{
		const auto scaled = fixed::downscale<3>(fixed::make::p<3>(0, 0));
		assert(fixed::accuracy(scaled) == 3);
	}
	{
		const auto scaled = fixed::downscale<1>(fixed::make::p<3>(0, 0));
		assert(fixed::accuracy(scaled) == 1);
	}
}

void test_accuracy_upscale()
{
	{
		const auto scaled = fixed::upscale<3>(fixed::make::p<3>(0, 0));
		assert(fixed::accuracy(scaled) == 3);
	}
	{
		const auto scaled = fixed::upscale<5>(fixed::make::p<3>(0, 0));
		assert(fixed::accuracy(scaled) == 5);
	}
}

void test_accuracy_downscale_upscale_downscale()
{
	const auto origin = fixed::make::p<3>(1, 111);
	assert(fixed::is_accurate(origin));
	assert(fixed::accuracy(origin) == 3);

	// downscale to lower precision than redownscale
	const auto downscaled = fixed::downscale<1>(origin);
	assert(!fixed::is_accurate(downscaled));
	assert(fixed::accuracy(downscaled) == 1);

	// upscale to higher precision
	const auto upscaled = fixed::upscale<4>(downscaled);
	assert(!fixed::is_accurate(upscaled));
	assert(fixed::accuracy(upscaled) == 1);

	// downscale again
	const auto redownscaled = fixed::downscale<2>(upscaled);
	assert(!fixed::is_accurate(redownscaled));
	assert(fixed::accuracy(redownscaled) == 1); // accuracy should be the same as in lower downscale from before
}

void test_accuracy_add()
{
	// when both numbers are accurate, resulting accuracy is the precision
	// of the result
	assert(fixed::accuracy(fixed::add(
		fixed::make::p<1>(0, 0), fixed::make::p<3>(0, 0))) == 3);

	// when one number is inaccurate, resulting accuracy should
	// inherit lowest accuracy
	{
		const auto x = fixed::make::p<2>(0, 11);
		assert(fixed::is_accurate(x));
		const auto x_scaled = fixed::downscale<1>(x);
		assert(!fixed::is_accurate(x_scaled));
		assert(fixed::accuracy(x_scaled) == 1);
		const auto y = fixed::make::p<3>(0, 111);
		assert(fixed::is_accurate(y));

		const auto result = fixed::add(x_scaled, y);
		assert(fixed::accuracy(result) == 1);
	}
}

void test_accuracy_sub()
{
	// when both numbers are accurate, resulting accuracy is the precision
	// of the result
	assert(fixed::accuracy(fixed::sub(
		fixed::make::p<1>(0, 0), fixed::make::p<3>(0, 0))) == 3);

	// when one number is inaccurate, resulting accuracy should
	// inherit lowest accuracy
	{
		const auto x = fixed::make::p<2>(0, 11);
		assert(fixed::is_accurate(x));
		const auto x_scaled = fixed::downscale<1>(x);
		assert(!fixed::is_accurate(x_scaled));
		assert(fixed::accuracy(x_scaled) == 1);
		const auto y = fixed::make::p<3>(0, 111);
		assert(fixed::is_accurate(y));

		const auto result = fixed::sub(x_scaled, y);
		assert(fixed::accuracy(result) == 1);
	}
}

void test_accuracy_mul()
{
	// when both numbers are accurate, resulting accuracy is the precision
	// of the result
	assert(fixed::accuracy(fixed::mul(
		fixed::make::p<2>(0, 0), fixed::make::p<3>(0, 0))) == 5);

	// when one number is inaccurate, resulting accuracy should
	// inherit lowest accuracy
	{
		const auto x = fixed::make::p<2>(0, 11);
		assert(fixed::is_accurate(x));
		const auto x_scaled = fixed::downscale<1>(x);
		assert(!fixed::is_accurate(x_scaled));
		assert(fixed::accuracy(x_scaled) == 1);
		const auto y = fixed::make::p<3>(0, 111);
		assert(fixed::is_accurate(y));

		const auto result = fixed::mul(x_scaled, y);
		assert(fixed::accuracy(result) == 1);
	}
}

void test_accuracy_div()
{
	// when both numbers are accurate, resulting accuracy is the precision
	// of the result
	{
		const fixed::detail::is_fixed_r auto r = fixed::div(
			fixed::make::p<1>(1, 0), fixed::make::p<2>(1, 0));
		assert(fixed::accuracy(fixed::get<3>(r)) == 3);
		assert(fixed::accuracy(fixed::get<2>(r)) == 2);
		assert(fixed::accuracy(fixed::get<1>(r)) == 1);
		assert(fixed::accuracy(fixed::get<0>(r)) == 0);
	}

	// when one number is inaccurate, resulting accuracy should
	// inherit lowest accuracy
	{
		const auto x = fixed::make::p<3>(1, 111);
		const auto x_scaled = fixed::downscale<1>(x);
		assert(!fixed::is_accurate(x_scaled));
		const fixed::detail::is_fixed_r auto r = fixed::div(
			x_scaled, fixed::make::p<2>(1, 0));
		assert(fixed::accuracy(fixed::get<3>(r)) == 1);
		assert(fixed::accuracy(fixed::get<2>(r)) == 1);
		assert(fixed::accuracy(fixed::get<1>(r)) == 1);
		assert(fixed::accuracy(fixed::get<0>(r)) == 0); // however accuracy is 0 when precision is 0
	}
}
