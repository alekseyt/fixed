#include <cassert>
#include <fixed/fixed.hpp>
#include <tests/utils.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

void test_add()
{
	// adding numbers with same precision
	// should keep precision of arguments
	{
		const auto result = (fixed::add(fixed::make::p<1>(100, 5),
			fixed::make::p<1>(50, 2)));
		static_assert(result.precision_v == 1);
		assert(result.value == 1507); // 150.7
	}
	// adding numbers with different precision should
	// change precision of result
	{
		const auto result = (fixed::add(fixed::make::p<1>(100, 5),
			fixed::add(fixed::make::p<1>(50, 2),
				fixed::make::p<2>(41, 25))));
		static_assert(result.precision_v == 2);
		assert(result.value == 191'95); // 191.95
	}
}

void test_add_accuracy()
{
	// both anumbers are accurate
	{
		const auto result = (fixed::add(fixed::make::p<1>(1, 0),
			fixed::make::p<2>(1, 0)));
		assert(fixed::is_accurate(result));
	}
	// one inaccurate number
	{
		const auto result = (fixed::add(fixed::make::p<1>(1, 0, false),
			fixed::make::p<2>(1, 0)));
		assert(!fixed::is_accurate(result));
	}
	// another inaccurate number
	{
		const auto result = (fixed::add(fixed::make::p<1>(1, 0),
			fixed::make::p<2>(1, 0, false)));
		assert(!fixed::is_accurate(result));
	}
	// both numbers inaccurate
	{
		const auto result = (fixed::add(fixed::make::p<1>(1, 0, false),
			fixed::make::p<2>(1, 0, false)));
		assert(!fixed::is_accurate(result));
	}
}

void test_add_overflow()
{
	{
		const auto result = fixed::add(fixed::make::p<1, uint8_t>((uint8_t)(1), 0),
			fixed::make::p<1, uint8_t>((uint8_t)(20), 0));
		assert(result.value == 210); // 10 + 200 -> 210
	}

	assert_throws<fixed::overflow_error>([](){ // 100 + 200 -> 300 (overflow)
		fixed::add(fixed::make::p<1, uint8_t>((uint8_t)(10), 0),
			fixed::make::p<1, uint8_t>((uint8_t)(20), 0));
	});
}

void test_add_negative()
{
	{
		const auto result = (fixed::add(fixed::make::p<1>(1, 5),
			fixed::make::p<2>(-2, 5)));
		assert(result.value == -100); // 1.5 + (-2.50) = -1.00
	}
	{
		const auto result = (fixed::add(fixed::make::p<1>(-1, 5),
			fixed::make::p<2>(-2, 5)));
		assert(result.value == -400); // -1.5 + (-2.50) = -4.00
	}
	{
		const auto result = (fixed::add(fixed::make::p<1>(-1, 5),
			fixed::make::p<2>(2, 5)));
		assert(result.value == 100); // -1.5 + 2.50 = 1.00
	}
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
