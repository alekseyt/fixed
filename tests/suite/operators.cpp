#include <cassert>
#include <fixed/fixed.hpp>

void test_opers_plus_raws()
{
	const fixed::detail::is_fixed_p auto x = fixed::make::p<2>(12, 34);
	const fixed::detail::is_fixed_p auto y = fixed::make::p<3>(56, 789);
	const fixed::detail::is_fixed_p auto z = x + y;

	assert(z.value == 69129);
}

void test_opers_minus_raws()
{
	const fixed::detail::is_fixed_p auto x = fixed::make::p<2>(3, 4);
	const fixed::detail::is_fixed_p auto y = fixed::make::p<3>(1, 2);
	const fixed::detail::is_fixed_p auto z = x - y;

	assert(z.value == 2200);
}

void test_opers_multiply_raws()
{
	const fixed::detail::is_fixed_p auto x = fixed::make::p<2>(1, 2);
	const fixed::detail::is_fixed_p auto y = fixed::make::p<3>(3, 4);
	const fixed::detail::is_fixed_p auto z = x * y;

	assert(z.value == 408000);
}

void test_opers_divide_raws()
{
	const fixed::detail::is_fixed_p auto x = fixed::make::p<2>(1, 2);
	const fixed::detail::is_fixed_p auto y = fixed::make::p<3>(3, 4);
	const fixed::detail::is_fixed_r auto z = x / y;

	const fixed::detail::is_fixed_p auto zz = fixed::get<2>(z);
	assert(zz.value == 35);
}
