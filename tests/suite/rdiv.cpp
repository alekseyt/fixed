#include <cassert>
#include <fixed/fixed.hpp>

void test_ratios_div()
{
	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<1>(4, 0) / fixed::make::p<2>(7, 0); // 4/7
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(2, 0) / fixed::make::p<4>(5, 0); // 2/5

	const fixed::detail::is_fixed_r auto x =
		fixed::div(a, b); // 4/7*2/5=20/14

	assert(fixed::detail::get(x.dividend) == 10'000'00); // 10.00000: precision 1+4
	assert(fixed::detail::get(x.divider) == 7'000'00); // 7.00000: precision 2+3
}

void test_ratios_div_simplify()
{
	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<1>(4, 0) / fixed::make::p<2>(7, 0); // 4/7
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(2, 0) / fixed::make::p<4>(5, 0); // 2/5

	{ // using simplication
		const fixed::detail::is_fixed_r auto sum = fixed::div(a, b);
		assert(fixed::detail::get(sum.dividend) == 10'000'00); // 10.00000
		assert(fixed::detail::get(sum.divider) == 7'000'00); // 7.00000
	}
	{ // not using simplification
		const fixed::detail::is_fixed_r auto sum = fixed::div(a, b, false);
		assert(fixed::detail::get(sum.dividend) == 20'000'00); // 20.00000
		assert(fixed::detail::get(sum.divider) == 14'000'00); // 14.00000
	}
}
