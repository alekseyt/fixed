#include <cassert>
#include <fixed/fixed.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

void test_compare_same_signness_less_more()
{
	const auto a = fixed::make::p<1>(1, 0);
	const auto b = fixed::make::p<2>(1, 1);

	assert(a < b);
	assert(b > a);

	const auto c = fixed::make::p<1>(-1, 0);
	const auto d = fixed::make::p<2>(-1, 1);

	assert(c > d);
	assert(d < c);
}

void test_compare_same_signness_equal_unequal()
{
	const auto a = fixed::make::p<1>(1, 0);
	const auto b = fixed::make::p<2>(1, 0);

	assert(a == b);

	const auto c = fixed::make::p<1>(-1, 0);

	assert(a != c);

	const auto d = fixed::make::p<1>(1, 1);

	assert(a != d);
}

void test_compare_same_signness_accuracy()
{
	auto a = fixed::make::p<1>(1, 0);
	auto b = fixed::make::p<2>(1, 0); // same as a with different precision
	b.accurate = false;              // but inaccurate
	assert (!fixed::is_accurate(b));
	auto c = fixed::make::p<1>(1, 0); // same as a
	c.accurate = false;              // but inaccurate
	assert (!fixed::is_accurate(c));

	assert(a != b);
	assert(b != c);
	assert(a != c);

	auto d = fixed::make::p<1>(1, 5);
	d.accurate = false;
	assert (!fixed::is_accurate(d));
	auto e = fixed::make::p<1>(1, 6);
	e.accurate = false;

	assert(d != e);
	assert(d < e && e > d); // while inaccurate, should be able to d < e
}

void test_compare_diff_signness_less_more()
{
	const auto a = fixed::make::p<1>(1, 0);
	const auto b = fixed::make::p<2, uint64_t>(1, 1);

	assert(a < b);
	assert(b > a);

	const auto c = fixed::make::p<1, uint64_t>(1, 0);
	const auto d = fixed::make::p<2>(-1, 1);

	assert(c > d);
	assert(d < c);
}

void test_compare_diff_signness_equal_unequal()
{
	const auto a = fixed::make::p<1>(1, 0);
	const auto b = fixed::make::p<2, uint64_t>(1, 0);

	assert(a == b);

	const auto c = fixed::make::p<1>(-1, 0);

	assert(a != c);

	const auto d = fixed::make::p<1>(1, 1);

	assert(a != d);
}

void test_compare_diff_signness_mix_types()
{
	const auto a = fixed::make::p<1, int8_t>((int8_t)(1), 0);
	const auto b = fixed::make::p<2, uint64_t>(1, 0);

	assert(a == b);

	const auto c = fixed::make::p<1, int8_t>((int8_t)(0), 9);

	assert(c < b);
	assert(b > c);
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
