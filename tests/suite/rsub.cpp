#include <cassert>
#include <fixed/fixed.hpp>

void test_ratios_sub()
{
	const fixed::detail::is_fixed_r auto origin =
		fixed::make::p<0>(1, 0) / fixed::make::p<0>(1, 0); // 1

	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<1>(1, 0) / fixed::make::p<2>(3, 0); // 1/3
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(1, 0) / fixed::make::p<4>(3, 0); // 1/3

	const fixed::detail::is_fixed_r auto x =
		fixed::sub(fixed::sub(origin, a), b); // 1-1/3-1/3=1/3 (3/9)

	const fixed::detail::is_fixed_r auto c =
		fixed::make::p<5>(1, 0) / fixed::make::p<6>(3, 0); // 1/3

	const fixed::detail::is_fixed_r auto y =
		fixed::sub(x, c); // 1/3-1/3=0 (0/27)

	const fixed::detail::is_fixed_p auto p = fixed::get<3>(y);
	assert(fixed::detail::get(p) == 0); // 0
	assert(fixed::is_accurate(p));
}

void test_ratios_sub_same_precision()
{
	const fixed::detail::is_fixed_r auto origin =
		fixed::make::p<0>(1, 0) / fixed::make::p<0>(1, 0); // 1

	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<3>(1, 0) / fixed::make::p<3>(3, 0); // 1/3
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(1, 0) / fixed::make::p<3>(3, 0); // 1/3
	const fixed::detail::is_fixed_r auto c =
		fixed::make::p<3>(1, 0) / fixed::make::p<3>(3, 0); // 1/3

	const fixed::detail::is_fixed_r auto sum =
		fixed::sub(fixed::sub(fixed::sub(origin, a), b), c);

	const fixed::detail::is_fixed_p auto p = fixed::get<3>(sum);
	assert(fixed::detail::get(p) == 0); // 0
	assert(fixed::is_accurate(p));
	assert(fixed::accuracy(p) == 3);
}

void test_ratios_sub_simplify()
{
	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<1>(5, 0) / fixed::make::p<2>(2, 0); // 5/2
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(1, 0) / fixed::make::p<4>(2, 0); // 1/2

	{ // using simplication
		const fixed::detail::is_fixed_r auto sum = fixed::sub(a, b);
		assert(fixed::detail::get(sum.dividend) == 2'000'00); // 2.00000
		assert(fixed::detail::get(sum.divider) == 1'000'000); // 1.000000
	}
	{ // not using simplification
		const fixed::detail::is_fixed_r auto sum = fixed::sub(a, b, false);
		assert(fixed::detail::get(sum.dividend) == 8'000'00); // 8.00000
		assert(fixed::detail::get(sum.divider) == 4'000'000); // 4.000000
	}
}
