#include <cassert>
#include <fixed/fixed.hpp>

// NOLINTBEGIN(hicpp-static-assert)

void test_decompose_integer()
{
	assert(fixed::detail::integer(fixed::make::p<2>(11, 7)) == 11);
	assert(fixed::detail::integer(fixed::make::p<3>(1, 3)) == 1);
	assert(fixed::detail::integer(fixed::make::p<0>(3, 0)) == 3);
	assert(fixed::detail::integer(fixed::make::p<2>(-11, 7)) == -11);
	assert(fixed::detail::integer(fixed::make::p<3>(-1, 3)) == -1);
	assert(fixed::detail::integer(fixed::make::p<0>(-3, 0)) == -3);
}

void test_decompose_fractional()
{
	assert(fixed::detail::fractional(fixed::make::p<0>(11, 7)).value == 0);
	assert(fixed::detail::fractional(fixed::make::p<1>(11, 7)).value == 7);
	assert(fixed::detail::fractional(fixed::make::p<2>(11, 7)).value == 70);
	assert(fixed::detail::fractional(fixed::make::p<2>(11, 77)).value == 77);

	assert(fixed::detail::fractional(fixed::make::p<0>(-11, 7)).value == 0);
	assert(fixed::detail::fractional(fixed::make::p<1>(-11, 7)).value == 7);
	assert(fixed::detail::fractional(fixed::make::p<2>(-11, 7)).value == 70);
	assert(fixed::detail::fractional(fixed::make::p<2>(-11, 77)).value == 77);
}

// NOLINTEND(hicpp-static-assert)
