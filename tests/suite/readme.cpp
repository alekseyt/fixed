#include <cassert>
#include <fixed/fixed.hpp>

void test_readme_how_to_use()
{
	assert(fixed::make::p<2>(1, 11).value == 111);
	assert(fixed::make::p<2>(1, 1).value == 110);
	assert(fixed::make::p<2>(1, fixed::make::f<1>(1)).value == 101);
}

void test_readme_compile_time_precision_checks()
{
	// compile error
	// fixed::make::p<3, uint8_t>((uint8_t)0, 0);
	// compile error
	// fixed::make::p<1, uint8_t>((uint8_t)0, 0) * fixed::make::p<2, uint8_t>((uint8_t)0, 0);
}
