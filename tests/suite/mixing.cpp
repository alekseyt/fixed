#include <cassert>
#include <fixed/fixed.hpp>
#include <tests/utils.hpp>

// static checks
static_assert(std::same_as<int32_t, fixed::detail::common_storage_t<fixed::p<2, uint16_t>, fixed::p<2, int32_t>>>);
static_assert(std::same_as<uint64_t, fixed::detail::common_storage_t<fixed::p<2, uint64_t>, fixed::p<2, int32_t>>>);
static_assert(std::same_as<uint64_t, fixed::detail::common_storage_t<fixed::p<2, uint64_t>, fixed::p<2, int64_t>>>);

void test_mixing_types_add_uint64_int64_underflow()
{
	const fixed::detail::is_fixed_p auto a = fixed::make::p<2, uint64_t>(0, 0);
	const fixed::detail::is_fixed_p auto b = fixed::make::p<3, int64_t>(-1, 0);

	assert_throws<fixed::overflow_error>([&](){
		const fixed::detail::is_fixed_p auto x = fixed::add(a, b);
		// result type is uint64_t, 0 + (-1) would be underflow, therefore x is empty
		static_assert(std::same_as<uint64_t, decltype(x)::storage_t>);
	});
}

void test_mixing_types_add_uint64_int64()
{
	const fixed::detail::is_fixed_p auto a = fixed::make::p<3, uint64_t>(1, 0);
	const fixed::detail::is_fixed_p auto b = fixed::make::p<2, int64_t>(-1, 0);
	const fixed::detail::is_fixed_p auto x = fixed::add(a, b);

	// result type is uint64_t, but 1 + (-1) = 0 therefore no underflow
	static_assert(std::same_as<uint64_t, decltype(x)::storage_t>);
	assert(x.value == 0);
}

void test_mixing_types_sub_uint64_int64_underflow()
{
	const fixed::detail::is_fixed_p auto a = fixed::make::p<2, uint64_t>(0, 0);
	const fixed::detail::is_fixed_p auto b = fixed::make::p<3, int64_t>(1, 0);

	assert_throws<fixed::overflow_error>([&](){
		const fixed::detail::is_fixed_p auto x = fixed::sub(a, b);
		// result type is uint64_t, 0 - 1 would be underflow, therefore x is empty
		static_assert(std::same_as<uint64_t, decltype(x)::storage_t>);
	});
}

void test_mixing_types_sub_uint64_int64()
{
	const fixed::detail::is_fixed_p auto a = fixed::make::p<3, uint64_t>(1, 0);
	const fixed::detail::is_fixed_p auto b = fixed::make::p<2, int64_t>(1, 0);
	const fixed::detail::is_fixed_p auto x = fixed::sub(a, b);

	// result type is uint64_t, but 1 - 1 = 0 therefore no underflow
	static_assert(std::same_as<uint64_t, decltype(x)::storage_t>);
	assert(x.value == 0);
}

void test_mixing_types_mul_uint64_int64_underflow()
{
	const fixed::detail::is_fixed_p auto a = fixed::make::p<1, uint64_t>(2, 0);
	const fixed::detail::is_fixed_p auto b = fixed::make::p<2, int64_t>(-3, 0);

	assert_throws<fixed::overflow_error>([&](){
		const fixed::detail::is_fixed_p auto x = fixed::mul(a, b);
		// result type is uint64_t, but result value would be negative, therefore underflow
		static_assert(std::same_as<uint64_t, decltype(x)::storage_t>);
	});
}

void test_mixing_types_mul_uint64_int64()
{
	const fixed::detail::is_fixed_p auto a = fixed::make::p<1, uint64_t>(3, 0);
	const fixed::detail::is_fixed_p auto b = fixed::make::p<2, int64_t>(2, 0);
	const fixed::detail::is_fixed_p auto x = fixed::mul(a, b);

	// result type is uint64_t, but result value is positive, therefore no underflow
	static_assert(std::same_as<uint64_t, decltype(x)::storage_t>);
	assert(x.value == 6000); // 6.000
}

void test_mixing_types_div_uint64_int64_underflow()
{
	const fixed::detail::is_fixed_p auto a = fixed::make::p<1, uint64_t>(1, 0);
	const fixed::detail::is_fixed_p auto b = fixed::make::p<2, int64_t>(-1, 0);
	const fixed::detail::is_fixed_r auto x = fixed::div(a, b);

	assert_throws<fixed::overflow_error>([&](){
		const fixed::detail::is_fixed_p auto y = fixed::get<2>(x);
		// result type is uint64_t, but result value is negative, therefore underflow
		static_assert(std::same_as<uint64_t, decltype(y)::storage_t>);
	});
}

void test_mixing_types_div_uint64_int64()
{
	const fixed::detail::is_fixed_p auto a = fixed::make::p<1, uint64_t>(1, 0);
	const fixed::detail::is_fixed_p auto b = fixed::make::p<2, int64_t>(2, 0);
	{
		const fixed::detail::is_fixed_r auto x = fixed::div(a, b);
		const fixed::detail::is_fixed_p auto y = fixed::get<2>(x);

		// result type is uint64_t, but result value is positive, therefore no underflow
		static_assert(std::same_as<uint64_t, decltype(y)::storage_t>);
		assert(y.value == 50); // 0.5
	}
	{
		// invert operatnds order
		const fixed::detail::is_fixed_r auto x = fixed::div(b, a);
		const fixed::detail::is_fixed_p auto y = fixed::get<3>(x);

		// result type is uint64_t, but result value is positive, therefore no underflow
		static_assert(std::same_as<uint64_t, decltype(y)::storage_t>);
		assert(y.value == 2000); // 2.000
	}
}
