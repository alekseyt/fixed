#include <cassert>
#include <fixed/fixed.hpp>
#include <tests/utils.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

// static checks
static_assert(fixed::detail::precision_to_scale<int, 0>() == 1);
static_assert(fixed::detail::precision_to_scale<int, 1>() == 1'0);
static_assert(fixed::detail::precision_to_scale<int, 2>() == 1'00);
static_assert(fixed::detail::precision_to_scale<int, 5>() == 1'000'00);
static_assert(fixed::detail::precision_to_scale<int, 9>() == 1'000'000'000);

static_assert(fixed::detail::can_upscale<uint8_t, 1, 1>);
static_assert(fixed::detail::can_upscale<uint8_t, 1, 2>);
static_assert(!fixed::detail::can_upscale<uint8_t, 2, 3>); // scale overflow
static_assert(fixed::detail::can_upscale<uint16_t, 2, 3>);
static_assert(fixed::detail::can_upscale<uint8_t, 0, 0>);
static_assert(fixed::detail::can_upscale<uint8_t, 0, 1>);
static_assert(!fixed::detail::can_upscale<uint8_t, 1, 0>); // downscale
static_assert(!fixed::detail::can_upscale<uint8_t, 2, 1>); // downscale

static_assert(fixed::detail::can_scale_to_precision<uint8_t, 0>());
static_assert(fixed::detail::can_scale_to_precision<uint8_t, 1>());
static_assert(fixed::detail::can_scale_to_precision<uint8_t, 2>());
static_assert(!fixed::detail::can_scale_to_precision<uint8_t, 3>()); // scale overflow
static_assert(fixed::detail::can_scale_to_precision<uint16_t, 3>());
static_assert(fixed::detail::can_scale_to_precision<int32_t, 9>());
static_assert(fixed::detail::can_scale_to_precision<uint64_t, fixed::limits::precision<uint64_t>::max()>());
static_assert(!fixed::detail::can_scale_to_precision<uint64_t, fixed::limits::precision<uint64_t>::max()+1>());

void test_scaling_upscale()
{
	fixed::detail::is_fixed_p auto p1 = fixed::make::p<2>(1, 55);
	const fixed::detail::is_fixed_p auto p2 = fixed::make::p<1>(1, 7);

	const auto upscaled = fixed::upscale<2>(p2);

	p1 = upscaled;

	assert(p1.value == 170); // 1.70
	assert(p1.accurate);
}

void test_scaling_upscale_overflow()
{
	const auto x = fixed::make::p<0, int64_t>(std::numeric_limits<int64_t>::max(), 0);
	assert_throws<fixed::overflow_error>([&x](){ fixed::upscale<1>(x); }); // x is already at max bits of storage
}

void test_scaling_upscale_accuracy()
{
	auto x = fixed::make::p<1>(1, 0);
	assert(fixed::is_accurate(x));
	const auto y = fixed::upscale<2>(x);
	assert(fixed::is_accurate(y));

	x.accurate = false;
	assert(!fixed::is_accurate(x));
	const auto z = fixed::upscale<2>(x);
	assert(!fixed::is_accurate(z));
}

void test_scaling_downscale()
{
	fixed::detail::is_fixed_p auto p1 = fixed::make::p<1>(1, 5);
	const fixed::detail::is_fixed_p auto p2 = fixed::make::p<2>(1, 77);

	const auto downscaled = fixed::downscale<1>(p2);

	p1 = downscaled;

	assert(p1.value == 17); // 1.7
	assert(!p1.accurate); // 1.77 -> 1.7 -> inaccurate
}

void test_scaling_downscale_loseless()
{
	fixed::detail::is_fixed_p auto p1 = fixed::make::p<1>(1, 5);
	const fixed::detail::is_fixed_p auto p2 = fixed::make::p<2>(1, 70);

	const auto downscaled = fixed::downscale<1>(p2);

	p1 = downscaled;

	assert(p1.value == 17); // 1.7 == 1.70 -> loseless
	assert(p1.accurate); // loseless -> accurate
}

void test_scaling_downscale_accuracy()
{
	auto x = fixed::make::p<2>(1, 0);
	assert(fixed::is_accurate(x));
	const auto y = fixed::downscale<1>(x);
	assert(fixed::is_accurate(y));

	x.accurate = false;
	assert(!fixed::is_accurate(x));
	const auto z = fixed::downscale<1>(x); // trim is loseless, but result should inherit accuracy flag
	assert(!fixed::is_accurate(z));
}

void test_scaling_precision_overflow()
{
	assert_no_throw([](){ fixed::detail::upscale(1, 2, 1); });
	assert_no_throw([](){ fixed::detail::upscale(1, fixed::limits::precision<int>::max(), 0); });
	assert_no_throw([](){ fixed::detail::upscale(1, fixed::limits::precision<int>::max(), 1); });
	assert_throws<fixed::overflow_error>([](){
		fixed::detail::upscale(1, fixed::limits::precision<int>::max() + 1, 1);
	});

	bool loseless = {};
	assert_no_throw([&loseless](){ fixed::detail::trim(loseless, 2, 1, 1); });
	assert_no_throw([&loseless](){ fixed::detail::trim(loseless, fixed::limits::precision<int>::max(), 1, 1); });
	assert_throws<fixed::overflow_error>([&loseless](){
		fixed::detail::trim(loseless, fixed::limits::precision<int>::max() + 1, 1, 1);
	});
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
