#include <cassert>
#include <fixed/fixed.hpp>
#include <tests/utils.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

void test_mul()
{
	// multiplying numbers with single precision
	// increases precision of result
	// 100.3 * 50.2 -> 5035.06
	{
		const auto result = fixed::mul(fixed::make::p<1>(100, 3),
			fixed::make::p<1>(50, 2));
		static_assert(result.precision_v == 2);
		assert(result.value == 5035'06); // 5035.06
	}
	// multiplying numbers with different precision
	// should produce result with presicion equal
	// to product of operands
	// e.g. 2.34 * 1.234 have scales 0.01 and 0.001
	// product is 0.01 * 0.001 = 0.00001
	// i.e. precision 5
	{
		const auto result = fixed::mul(fixed::make::p<2>(2, 34),
			fixed::make::p<3>(1, 234));
		static_assert(result.precision_v == 5);
		assert(result.value == 2'887'56); // 2.88756
	}

	{
		const auto result = fixed::mul(fixed::make::p<5>(12, 34567),
			fixed::make::p<4>(3, 4567));
		static_assert(result.precision_v == 9);
		assert(result.value == 42'675'277'489); // 42.675277489
	}
}

void test_mul_accuracy()
{
	// both numbers are accurate
	{
		const auto result = fixed::mul(fixed::make::p<1>(1, 0),
			fixed::make::p<2>(1, 0));
		assert(fixed::is_accurate(result));
	}
	// one number inaccurate
	{
		const auto result = fixed::mul(fixed::make::p<1>(1, 0, false),
			fixed::make::p<2>(1, 0));
		assert(!fixed::is_accurate(result));
	}
	// another number inaccurate
	{
		const auto result = fixed::mul(fixed::make::p<1>(1, 0),
			fixed::make::p<2>(1, 0, false));
		assert(!fixed::is_accurate(result));
	}
	// both numbers inaccurate
	{
		const auto result = fixed::mul(fixed::make::p<1>(1, 0, false),
			fixed::make::p<2>(1, 0, false));
		assert(!fixed::is_accurate(result));
	}
}

void test_mul_overflow()
{
	{
		const auto result = fixed::mul(fixed::make::p<1, uint8_t>((uint8_t)(1), 0),
			fixed::make::p<1, uint8_t>((uint8_t)(1), 0));
		assert(result.value == 100); // 10 * 10 -> 100
	}

	assert_throws<fixed::overflow_error>([](){ // 50 * 50 -> 2500 (overflow)
		fixed::mul(fixed::make::p<1, uint8_t>((uint8_t)(5), 0),
			fixed::make::p<1, uint8_t>((uint8_t)(5), 0));
	});
}

void test_mul_negative()
{
	{
		const auto result = fixed::mul(fixed::make::p<1>(2, 0),
			fixed::make::p<2>(-1, 5));
		assert(result.value == -3000);
	}
	{
		const auto result = fixed::mul(fixed::make::p<1>(-2, 0),
			fixed::make::p<2>(-1, 5));
		assert(result.value == 3000);
	}
	{
		const auto result = fixed::mul(fixed::make::p<1>(-2, 0),
			fixed::make::p<2>(1, 5));
		assert(result.value == -3000);
	}
}

void test_mul_15_by_27() // bug was discovered when multiplying 1.5 * 2.7, should be 4.05
{
	const auto result = fixed::mul(fixed::make::p<2, uint64_t>(1, 5),
		fixed::make::p<3>(2, 7));
	assert(result.value == 405000); // 4.05000 // it works fine, but leadin 0 was consumed
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
