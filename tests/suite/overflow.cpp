#include <cassert>
#include <fixed/fixed.hpp>
#include <fixed/detail/overflow.hpp>

// NOLINTBEGIN(hicpp-static-assert)

void test_overflow_mul()
{
	// unsigned
	assert(*fixed::detail::check_mul_overflow<uint8_t>(0, 0) == 0);
	assert(*fixed::detail::check_mul_overflow<uint8_t>(1, 1) == 1);
	assert(*fixed::detail::check_mul_overflow<uint8_t>(2, 3) == 6);
	assert(*fixed::detail::check_mul_overflow<uint8_t>(127, 2) == 254);
	assert(!fixed::detail::check_mul_overflow<uint8_t>(128, 2)); // 128 * 2 -> 256

	// signed
	assert(*fixed::detail::check_mul_overflow<int8_t>(0, 0) == 0);
	assert(*fixed::detail::check_mul_overflow<int8_t>(1, 1) == 1);
	assert(*fixed::detail::check_mul_overflow<int8_t>(63, 2) == 126);
	assert(!fixed::detail::check_mul_overflow<int8_t>(64, 2)); // 64 * 2 -> 128
}

void test_overflow_add()
{
	// unsigned
	assert(*fixed::detail::check_add_overflow<uint8_t>(0, 0) == 0);
	assert(*fixed::detail::check_add_overflow<uint8_t>(1, 1) == 2);
	assert(*fixed::detail::check_add_overflow<uint8_t>(127, 127) == 254);
	assert(!fixed::detail::check_add_overflow<uint8_t>(128, 128)); // 256

	// signed
	assert(*fixed::detail::check_add_overflow<int8_t>(0, 0) == 0);
	assert(*fixed::detail::check_add_overflow<int8_t>(1, 1) == 2);
	assert(*fixed::detail::check_add_overflow<int8_t>(126, 1) == 127);
	assert(!fixed::detail::check_add_overflow<int8_t>(127, 1)); // 128
}

void test_overflow_sub()
{
	// unsigned
	assert(*fixed::detail::check_sub_overflow<uint8_t>(0, 0) == 0);
	assert(*fixed::detail::check_sub_overflow<uint8_t>(1, 1) == 0);
	assert(!fixed::detail::check_sub_overflow<uint8_t>(1, 2));
	assert(!fixed::detail::check_sub_overflow<uint8_t>(0, 1));

	// signed
	assert(*fixed::detail::check_sub_overflow<int8_t>(0, 0) == 0);
	assert(*fixed::detail::check_sub_overflow<int8_t>(0, 1) == -1);
	assert(*fixed::detail::check_sub_overflow<int8_t>(0, 128) == -128);
	assert(!fixed::detail::check_sub_overflow<int8_t>(0, 129));
}

// NOLINTEND(hicpp-static-assert)
