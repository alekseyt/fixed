#include <cassert>
#include <fixed/fixed.hpp>

void test_ratios_add()
{
	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<1>(1, 0) / fixed::make::p<2>(3, 0); // 1/3
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(1, 0) / fixed::make::p<4>(3, 0); // 1/3

	const fixed::detail::is_fixed_r auto x =
		fixed::add(a, b); // 1/3+1/3=2/3 (6/9)

	const fixed::detail::is_fixed_r auto c =
		fixed::make::p<5>(1, 0) / fixed::make::p<6>(3, 0); // 1/3

	const fixed::detail::is_fixed_r auto y =
		fixed::add(x, c); // 2/3+1/3=3/3 (27/27)

	const fixed::detail::is_fixed_p auto p = fixed::get<3>(y);
	assert(fixed::detail::get(p) == 1'000); // 1.000
	assert(fixed::is_accurate(p));
}

void test_ratios_add_same_precision()
{
	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<3>(1, 0) / fixed::make::p<3>(3, 0); // 1/3
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(1, 0) / fixed::make::p<3>(3, 0); // 1/3
	const fixed::detail::is_fixed_r auto c =
		fixed::make::p<3>(1, 0) / fixed::make::p<3>(3, 0); // 1/3

	const fixed::detail::is_fixed_r auto sum =
		fixed::add(fixed::add(a, b), c);

	const fixed::detail::is_fixed_p auto p = fixed::get<3>(sum);
	assert(fixed::detail::get(p) == 1'000); // 1.000
	assert(fixed::is_accurate(p));
	assert(fixed::accuracy(p) == 3);
}

void test_ratios_add_simplify()
{
	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<1>(1, 0) / fixed::make::p<2>(2, 0); // 1/2
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(1, 0) / fixed::make::p<4>(2, 0); // 1/2

	{ // using simplication
		const fixed::detail::is_fixed_r auto sum = fixed::add(a, b);
		assert(fixed::detail::get(sum.dividend) == 1'000'00); // 1.00000
		assert(fixed::detail::get(sum.divider) == 1'000'000); // 1.000000
	}
	{ // not using simplification
		const fixed::detail::is_fixed_r auto sum = fixed::add(a, b, false);
		assert(fixed::detail::get(sum.dividend) == 4'000'00); // 4.00000
		assert(fixed::detail::get(sum.divider) == 4'000'000); // 4.000000
	}
}
