#include <cassert>
#include <fixed/fixed.hpp>

void test_ratios_simplify()
{
	{
		const fixed::detail::is_fixed_r auto x =
			fixed::make::p<1>(2, 0) / fixed::make::p<2>(4, 0); // 2/4
		const fixed::detail::is_fixed_r auto s =
			fixed::simplify(x);

		static_assert(fixed::precision(s.dividend) == 1);
		assert(fixed::detail::get(s.dividend) == 10); // 1.0
		static_assert(fixed::precision(s.divider) == 2);
		assert(fixed::detail::get(s.divider) == 200); // 2.00
	}
	{
		const fixed::detail::is_fixed_r auto x =
			fixed::make::p<1>(20, 0) / fixed::make::p<2>(14, 0); // 20/14
		const fixed::detail::is_fixed_r auto s =
			fixed::simplify(x);
		static_assert(fixed::precision(s.dividend) == 1);
		assert(fixed::detail::get(s.dividend) == 100); // 10.0
		static_assert(fixed::precision(s.divider) == 2);
		assert(fixed::detail::get(s.divider) == 700); // 7.00
	}
	{
		const fixed::detail::is_fixed_r auto x =
			fixed::make::p<1>(3, 0) / fixed::make::p<2>(9, 0); // 3/9
		const fixed::detail::is_fixed_r auto s =
			fixed::simplify(x);
		static_assert(fixed::precision(s.dividend) == 1);
		assert(fixed::detail::get(s.dividend) == 10); // 1.0
		static_assert(fixed::precision(s.divider) == 2);
		assert(fixed::detail::get(s.divider) == 300); // 3.00
	}

	// can not simplify
	{
		const fixed::detail::is_fixed_r auto x =
			fixed::make::p<1>(2, 0) / fixed::make::p<2>(5, 0); // 2/5
		const fixed::detail::is_fixed_r auto s =
			fixed::simplify(x);
		static_assert(fixed::precision(s.dividend) == 1);
		assert(fixed::detail::get(s.dividend) == 20); // 2.0
		static_assert(fixed::precision(s.divider) == 2);
		assert(fixed::detail::get(s.divider) == 500); // 5.00
	}
}
