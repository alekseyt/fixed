#include <cassert>
#include <fixed/fixed.hpp>
#include <tests/utils.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

void test_sub()
{
	// sub tests are similar to add
	// just with inverted sign
	{
		const auto result = (fixed::sub(fixed::make::p<1>(100, 5),
			fixed::make::p<1>(50, 2)));
		static_assert(result.precision_v == 1);
		assert(result.value == 50'3); // 50.3
	}
	// order of operands matters
	// operation is ((100.5 - 50.2) - 41.25) -> 9.05
	{
		const auto result = (fixed::sub(fixed::sub(fixed::make::p<1>(100, 5),
			fixed::make::p<1>(50, 2)),
				fixed::make::p<2>(41, 25)));
		static_assert(result.precision_v == 2);
		assert(result.value == 9'05); // 9.05
	}
	// another order of operands
	// (100.5 - (50.2 - 41.25)) -> 91.55
	{
		const auto result = (fixed::sub(fixed::make::p<1>(100, 5),
			fixed::sub(fixed::make::p<1>(50, 2),
				fixed::make::p<2>(41, 25))));
		static_assert(result.precision_v == 2);
		assert(result.value == 91'55); // 91.55
	}
}

void test_sub_accuracy()
{
	// both numbers are accurate
	{
		const auto result = (fixed::sub(fixed::make::p<1>(1, 0),
			fixed::make::p<2>(1, 0)));
		assert(fixed::is_accurate(result));
	}
	// one number inaccurate
	{
		const auto result = (fixed::sub(fixed::make::p<1>(1, 0, false),
			fixed::make::p<2>(1, 0)));
		assert(!fixed::is_accurate(result));
	}
	// another number inaccurate
	{
		const auto result = (fixed::sub(fixed::make::p<1>(1, 0),
			fixed::make::p<2>(1, 0, false)));
		assert(!fixed::is_accurate(result));
	}
	// both numbers inaccurate
	{
		const auto result = (fixed::sub(fixed::make::p<1>(1, 0, false),
			fixed::make::p<2>(1, 0, false)));
		assert(!fixed::is_accurate(result));
	}
}

void test_sub_signed()
{
	{
		const auto result = (fixed::sub(fixed::make::p<1>(100, 5),
			fixed::make::p<1>(200, 2)));
		static_assert(result.precision_v == 1);
		assert(result.value == -99'7); // 100.5 - 200.2 -> -99.7
	}
}

void test_sub_overflow()
{
	{
		const auto result = fixed::sub(fixed::make::p<1, uint8_t>((uint8_t)(2), 0),
			fixed::make::p<1, uint8_t>((uint8_t)(1), 0));
		assert(result.value == 10); // 20 - 10 -> 10
	}

	{
		const auto result = fixed::sub(fixed::make::p<1, uint8_t>((uint8_t)(1), 0),
			fixed::make::p<1, uint8_t>((uint8_t)(1), 0));
		assert(result.value == 0); // 10 - 10 -> 0
	}

	assert_throws<fixed::overflow_error>([]() { // 10 - 20 -> -10 (underflow)
		fixed::sub(fixed::make::p<1, uint8_t>((uint8_t)(10), 0),
			fixed::make::p<1, uint8_t>((uint8_t)(20), 0));
	});
}

void test_sub_negative()
{
	{
		const auto result = (fixed::sub(fixed::make::p<1>(1, 5),
			fixed::make::p<2>(-2, 5)));
		assert(result.value == 400); // 1.5 - (-2.50) = 4.00
	}
	{
		const auto result = (fixed::sub(fixed::make::p<1>(-1, 5),
			fixed::make::p<2>(-2, 5)));
		assert(result.value == 100); // -1.5 - (-2.50) = 1.00
	}
	{
		const auto result = (fixed::sub(fixed::make::p<1>(-1, 5),
			fixed::make::p<2>(2, 5)));
		assert(result.value == -400); // -1.5 - 2.50 = -4.00
	}
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
