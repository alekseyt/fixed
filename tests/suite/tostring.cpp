#include <cassert>
#include <fixed/fixed.hpp>

void test_to_string()
{
	assert(fixed::to_string(fixed::make::p<0>(0, 0)) == "0.");
	assert(fixed::to_string(fixed::make::p<1>(0, 0)) == "0.0");
	assert(fixed::to_string(fixed::make::p<2>(0, 0)) == "0.00");
	assert(fixed::to_string(fixed::make::p<3>(0, 0)) == "0.000");

	assert(fixed::to_string(fixed::make::p<0>(1, 1)) == "1.");
	assert(fixed::to_string(fixed::make::p<1>(1, 1)) == "1.1");
	assert(fixed::to_string(fixed::make::p<2>(10, fixed::make::f<1>(1))) == "10.01");

	assert(fixed::to_string(fixed::make::p<0>(-1, 0)) == "-1.");
	assert(fixed::to_string(fixed::make::p<1>(-1, 1)) == "-1.1");
	assert(fixed::to_string(fixed::make::p<2>(-1, 1)) == "-1.10");
	assert(fixed::to_string(fixed::make::p<3>(-1, fixed::make::f<1>(1))) == "-1.001");
}
