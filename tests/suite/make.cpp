#include <cassert>
#include <fixed/fixed.hpp>
#include <tests/utils.hpp>

// NOLINTBEGIN(hicpp-static-assert)

// static checks
static_assert(fixed::detail::i_compatible_with_storage<int, int>);
static_assert(fixed::detail::i_compatible_with_storage<int64_t, int64_t>);

static_assert(fixed::detail::i_compatible_with_storage<int, int64_t>);
static_assert(fixed::detail::i_compatible_with_storage<int, int32_t>);
static_assert(!fixed::detail::i_compatible_with_storage<int, int16_t>);
static_assert(!fixed::detail::i_compatible_with_storage<int, int8_t>);

static_assert(!fixed::detail::i_compatible_with_storage<int64_t, int32_t>);
static_assert(fixed::detail::i_compatible_with_storage<int32_t, int64_t>);

static_assert(fixed::detail::i_compatible_with_make<int, int64_t>);
static_assert(!fixed::detail::i_compatible_with_make<float, int64_t>);

void test_make()
{
	assert(fixed::make::p<0>(0, 0).value == 0);

	assert(fixed::make::p<0>(1, 0).value == 1);
	assert(fixed::make::p<1>(1, 2).value == 12);
	assert(fixed::make::p<2>(1, 23).value == 123);
	assert(fixed::make::p<2>(1, 10).value == 110);
	assert(fixed::make::p<3>(1, 234).value == 1234);
	assert(fixed::make::p<1>(0, 1).value == 1);
	assert(fixed::make::p<2>(0, 12).value == 12);
	assert(fixed::make::p<3>(0, 123).value == 123);
}

void test_make_with_f()
{
	assert(fixed::make::p<2>(1, 1).value == 110);
	assert(fixed::make::p<2>(1, fixed::make::f<1>(1)).value == 101);
	assert(fixed::make::p<2>(1, fixed::make::f<2>(1)).value == 110);
}

void test_make_accuracy()
{
	assert(fixed::make::p<0>(1, 0).accurate);
	assert(!fixed::make::p<0>(1, 1).accurate);
	assert(fixed::make::p<0>(3, 0).accurate);

	// explicitly provided accuracy flags
	assert(!fixed::make::p<0>(0, 0, false).accurate);
	assert(fixed::make::p<0>(0, 0, true).accurate);

	// non-zero fraction, but zero precision, but explicit accuracy flag
	assert(!fixed::make::p<0>(0, 1, true).accurate);
	assert(!fixed::make::p<0>(0, 1, false).accurate);
}

void test_make_precision_shift()
{
	assert(fixed::make::p<2>(0, 1).value == 10);
	assert(fixed::make::p<2>(0, 10).value == 10);

	assert(fixed::make::p<2>(0, fixed::make::f<2>(1)).value == 10);
	assert(fixed::make::p<2>(0, fixed::make::f<1>(1)).value == 1);

	assert(fixed::make::p<2>(0, 101).value == 10);
	assert(!fixed::make::p<2>(0, 101).accurate);
}

void test_make_loseless_trim()
{
	// trimming 0.100 to 0.10 is a loseless operation
	assert(fixed::make::p<2>(0, 100).value == 10);
	assert(fixed::make::p<2>(0, 100).accurate);
}

void test_make_negative()
{
	assert(fixed::make::p<2>(-0, 10).value == 10);
	assert(fixed::make::p<2>(-1, 10).value == -110);
	assert(fixed::make::p<0>(-1, 0).value == -1);
	assert(fixed::make::p<0>(-0, 0).value == 0);
}

void test_make_overflow_value()
{
	assert_no_throw([](){ fixed::make::p<0, uint8_t>(static_cast<uint8_t>(1), 0); }); // 1
	assert_no_throw([](){ fixed::make::p<1, uint8_t>(static_cast<uint8_t>(1), 0); }); // 10
	assert_no_throw([](){ fixed::make::p<2, uint8_t>(static_cast<uint8_t>(1), 0); }); // 100
	// assert((!fixed::make::p<3, uint8_t>(static_cast<uint8_t>(1), 0))); // 1000 // compile time error
}

void test_make_precision_max()
{
	assert_no_throw([](){ fixed::make::p<fixed::limits::precision<uint64_t>::max() - 1, uint64_t>(1, 1); });

	// p is at maximum possible precision
	assert_no_throw([](){ fixed::make::p<fixed::limits::precision<uint64_t>::max(), uint64_t>(0, 0); });
	assert_no_throw([](){ fixed::make::p<fixed::limits::precision<uint64_t>::max(), uint64_t>(1, 0); });
	assert_no_throw([](){ fixed::make::p<fixed::limits::precision<uint64_t>::max(), uint64_t>(1, 1); });

	// p is slightly over maximum possible precision
	// assert((!!fixed::make::p<fixed::limits::precision<uint64_t>::max()+1, uint64_t>(0, 0))); // compile time error
}

// NOLINTEND(hicpp-static-assert)
