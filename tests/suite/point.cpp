#include <cassert>
#include <fixed/fixed.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)
// NOLINTBEGIN(hicpp-static-assert)

void test_p_assignment()
{
	const fixed::p<2> origin = fixed::make::p<2>(123, 45);
	assert(origin.value == 12345);

	const fixed::p<3> x = fixed::upscale<3>(origin);
	assert(x.value == 123450);

	const fixed::p<2> y = fixed::upscale<2>(origin);
	assert(y.value == 12345);

	fixed::p<2> copy;
	assert(copy.value != 12345);
	copy = origin;
	assert(copy.value == 12345);
}

void test_p_accuracy()
{
	{
		const auto x = fixed::make::p<3>(12, 123);
		assert(x.value == 12123);
		assert(x.accurate);
		assert(fixed::is_accurate(x));
	}
	{
		const auto x = fixed::make::p<2>(12, 123);
		assert(x.value == 1212);
		assert(!x.accurate);
		assert(!fixed::is_accurate(x));
	}
	{
		const auto x = fixed::make::p<1>(12, 123);
		assert(x.value == 121);
		assert(!x.accurate);
		assert(!fixed::is_accurate(x));
	}
	{
		const auto x = fixed::make::p<0>(12, 123);
		assert(x.value == 12);
		assert(!x.accurate);
		assert(!fixed::is_accurate(x));
	}
}

void test_p_zero_precision()
{
	assert(fixed::make::p<2>(12, 0).value == 1200);
	assert(fixed::make::p<1>(12, 0).value == 120);
	assert(fixed::make::p<0>(12, 0).value == 12);
}

void test_p_scale()
{
	static_assert(fixed::detail::scale(fixed::p<0>()) == 1);
	static_assert(fixed::detail::scale(fixed::p<1>()) == 10);
	static_assert(fixed::detail::scale(fixed::p<2>()) == 100);
}

void test_p_scale_constexpr()
{
	constexpr unsigned scale = fixed::detail::scale(fixed::p<0>());
	static_assert(scale == 1);
}

// NOLINTEND(hicpp-static-assert)
// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
