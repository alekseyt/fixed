#include <cassert>
#include <fixed/limits.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

// static checks
static_assert(fixed::limits::detail::max_exp10<uint64_t, 0>() == 0);
static_assert(fixed::limits::detail::max_exp10<uint64_t, 9>() == 1);
static_assert(fixed::limits::detail::max_exp10<uint64_t, 99>() == 10);
static_assert(fixed::limits::detail::max_exp10<uint64_t, 999>() == 100);

static_assert(fixed::limits::detail::exp10_order<uint64_t, 0>() == 0);
static_assert(fixed::limits::detail::exp10_order<uint64_t, 1>() == 0);
static_assert(fixed::limits::detail::exp10_order<uint64_t, 10>() == 1);
static_assert(fixed::limits::detail::exp10_order<uint64_t, 100>() == 2);

static_assert(fixed::limits::detail::scale_max<uint8_t>() == 100);
static_assert(fixed::limits::detail::scale_max<uint16_t>() == 10000);

static_assert(fixed::limits::precision<uint8_t>::max() == 2); // scale is the limit, 100
static_assert(fixed::limits::precision<uint16_t>::max() == 4); // 10000

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
