#include <cassert>
#include <fixed/fixed.hpp>

// static checks
//static_assert(fixed::detail::is_fixed_f<fixed::f{}>);

void test_f_assignment()
{
	assert(fixed::make::f<1>(1).value == 1);
	assert(fixed::make::f<2>(1).value == 10);
	assert(fixed::make::f<2>(10).value == 10);
}

void test_f_accuracy()
{
	assert(fixed::make::f<2>(1).accurate);
	assert(fixed::make::f<2>(10).accurate);
	assert(!fixed::make::f<2>(101).accurate);
}

void test_f_props()
{
	const auto f = fixed::make::f<2>(123);
	(void)(f);
	static_assert(f.precision_v == 2);
	static_assert((f.scale_v == fixed::detail::precision_to_scale<fixed::detail::assumed_fractional_storage_t, 2>()));
}

void test_f_zero_precision()
{
	assert(fixed::make::f<0>(1).value == 0);
	assert(!fixed::make::f<0>(1).accurate); // trimming to zero precision produces zero
}

void test_f_loseless_trim()
{
	assert(fixed::make::f<2>(10).value == 10);

	// trimming 10, 100, 1000 to 1 is a loseless op
	assert(fixed::make::f<1>(10).value == 1);
	assert(fixed::make::f<1>(100).value == 1);
	assert(fixed::make::f<1>(1000).value == 1);
	assert(fixed::make::f<1>(1000).accurate);

	// trimming 11, 101, 1001 to 1 is a lossy op
	assert(!fixed::make::f<1>(11).accurate);
	assert(!fixed::make::f<1>(101).accurate);
	assert(!fixed::make::f<1>(1001).accurate);
}

void test_f_trimming()
{
	assert(fixed::make::f<3>(123).value == 123);
	assert(fixed::make::f<2>(123).value == 12);
	assert(fixed::make::f<1>(123).value == 1);

	assert(fixed::make::f<3>(103).value == 103);
	assert(fixed::make::f<2>(103).value == 10);
	assert(fixed::make::f<1>(103).value == 1);

	assert(fixed::make::f<3>(120).value == 120);
	assert(fixed::make::f<2>(120).value == 12);
	assert(fixed::make::f<1>(130).value == 1);

	assert(fixed::make::f<2>(12).value == 12);
	assert(fixed::make::f<1>(12).value == 1);
}
