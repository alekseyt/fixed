#include <cassert>
#include <fixed/fixed.hpp>

void test_ratios_mul()
{
	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<1>(2, 0) / fixed::make::p<2>(5, 0); // 2/5
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(3, 0) / fixed::make::p<4>(4, 0); // 3/4

	const fixed::detail::is_fixed_r auto x =
		fixed::mul(a, b); // 2/5*3/4=6/20

	assert(fixed::detail::get(x.dividend) == 3'000'0); // 3.0000: precision 1+3
	assert(fixed::detail::get(x.divider) == 10'000'000); // 10.000000: precision 2+4
}

void test_ratios_mul_simplify()
{
	const fixed::detail::is_fixed_r auto a =
		fixed::make::p<1>(2, 0) / fixed::make::p<2>(5, 0); // 2/5
	const fixed::detail::is_fixed_r auto b =
		fixed::make::p<3>(3, 0) / fixed::make::p<4>(4, 0); // 3/4

	{ // using simplication
		const fixed::detail::is_fixed_r auto sum = fixed::mul(a, b);
		assert(fixed::detail::get(sum.dividend) == 3'000'0); // 3.00000
		assert(fixed::detail::get(sum.divider) == 10'000'000); // 10.000000
	}
	{ // not using simplification
		const fixed::detail::is_fixed_r auto sum = fixed::mul(a, b, false);
		assert(fixed::detail::get(sum.dividend) == 6'000'0); // 6.00000
		assert(fixed::detail::get(sum.divider) == 20'000'000); // 20.000000
	}
}
