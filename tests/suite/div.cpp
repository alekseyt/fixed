#include <cassert>
#include <fixed/fixed.hpp>
#include <tests/utils.hpp>

void test_div()
{
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<4>(7, 7872), fixed::make::p<2>(3, 14));
		assert(fixed::get<2>(result).value == 248); // 2.48
	}

	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<4>(3, 0), fixed::make::p<2>(1, 5));
		assert(fixed::get<2>(result).value == 200); // 2.0
	}

	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<4>(3, 0), fixed::make::p<2>(2, 0));
		assert(fixed::get<3>(result).value == 1500); // 1.500
	}
}

void test_div_accuracy()
{
	// both numbers are accurate
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<1>(1, 0), fixed::make::p<2>(1, 0));
		const auto p = fixed::get<0>(result);
		assert(fixed::is_accurate(p));
	}
	// one number inaccurate
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<1>(1, 0, false), fixed::make::p<2>(1, 0));
		const auto p = fixed::get<0>(result);
		assert(!fixed::is_accurate(p));
	}
	// another number inaccurate
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<1>(1, 0), fixed::make::p<2>(1, 0, false));
		const auto p = fixed::get<0>(result);
		assert(!fixed::is_accurate(p));
	}
	// both numbers inaccurate
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<1>(1, 0, false), fixed::make::p<2>(1, 0, false));
		const auto p = fixed::get<0>(result);
		assert(!fixed::is_accurate(p));
	}
}

void test_div_inf()
{
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<0>(1, 0), fixed::make::p<0>(3, 0));
		assert(fixed::get<0>(result).value == 0); // 0
	}

	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<0>(1, 0), fixed::make::p<0>(3, 0));
		assert(fixed::get<1>(result).value == 3); // 0.3
	}

	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<0>(1, 0), fixed::make::p<0>(3, 0));
		assert(fixed::get<2>(result).value == 33); // 0.33
	}

	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<0>(1, 0), fixed::make::p<0>(3, 0));
		assert(fixed::get<5>(result).value == 33333); // 0.33333
	}
}

void test_div_exact()
{
	// 1/3 -> inaccurate with any precision
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<0>(1, 0), fixed::make::p<0>(3, 0));

		assert(!fixed::get<0>(result).accurate);
		assert(!fixed::get<1>(result).accurate);
		assert(!fixed::get<2>(result).accurate);
		assert(!fixed::get<5>(result).accurate);
	}

	// 4/2 -> accurate
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<0>(4, 0), fixed::make::p<0>(2, 0));

		assert(fixed::get<0>(result).accurate);
		assert(fixed::get<1>(result).accurate);
		assert(fixed::get<2>(result).accurate);
		assert(fixed::get<5>(result).accurate);
	}

	// 10/8 -> accurate with precision 2
	// 10/8 -> inaccurate with precision 1
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<0>(10, 0), fixed::make::p<0>(8, 0));

		assert(!fixed::get<0>(result).accurate); // 1
		assert(!fixed::get<1>(result).accurate); // 1.2
		assert(fixed::get<2>(result).accurate); // 1.25
		assert(fixed::get<3>(result).accurate); // 1.250
		assert(fixed::get<5>(result).accurate); // 1.25000
	}
}

void test_div_dbz()
{
	const fixed::detail::is_fixed_r auto result =
		fixed::div(fixed::make::p<0>(10, 0), fixed::make::p<0>(0, 0));
	assert_throws<fixed::domain_error>([&](){
		fixed::get<0>(result);
	});
}

void test_div_negative()
{
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<1>(-3, 0), fixed::make::p<1>(1, 5));
		const fixed::detail::is_fixed_p auto x = fixed::get<1>(result);
		assert(x.value == -20);
	}
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<1>(-3, 0), fixed::make::p<1>(-1, 5));
		const fixed::detail::is_fixed_p auto x = fixed::get<1>(result);
		assert(x.value == 20);
	}
	{
		const fixed::detail::is_fixed_r auto result =
			fixed::div(fixed::make::p<1>(3, 0), fixed::make::p<1>(-1, 5));
		const fixed::detail::is_fixed_p auto x = fixed::get<1>(result);
		assert(x.value == -20);
	}
}

void test_div_15_by_27() // bug
{
	const auto x = fixed::make::p<2, uint64_t>(1, 5);
	const auto y = fixed::make::p<3>(2, 7);

	const fixed::detail::is_fixed_r auto result = x / y;

	assert(fixed::get<1>(result).value == 5);
	assert(fixed::get<2>(result).value == 55);
	assert(fixed::get<3>(result).value == 555);
	assert(fixed::get<5>(result).value == 555'55);
	assert(fixed::get<9>(result).value == 555'555'555); // bug at precision 9
	assert(fixed::get<12>(result).value == 555'555'555'555);
}
