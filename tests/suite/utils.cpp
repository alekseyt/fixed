#include <cassert>
#include <fixed/fixed.hpp>
#include <tests/utils.hpp>

// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers)

void test_utils_upscale_comptime()
{
	assert((fixed::detail::upscale<2, 3>(234) == 2340));
	static_assert((fixed::detail::upscale<3, 3>(1234) == 1234));
}

void test_utils_upscale_runtime()
{
	static_assert((fixed::detail::upscale(3, 3, 1234)));
	assert_throws<fixed::invalid_argument>([](){ fixed::detail::upscale(3, 2, 1234); }); // can't downscale
}

void test_utils_upscale_overflow()
{
	assert(fixed::detail::upscale<uint8_t>(0, 1, 1) == 10);
	assert(fixed::detail::upscale<uint8_t>(1, 2, 10) == 100);
	assert(fixed::detail::upscale<uint8_t>(0, 1, 0) == 0);
	assert_throws<fixed::overflow_error>([](){ fixed::detail::upscale<uint8_t>(1, 3, 10); }); // 10 -> 1000
	assert_throws<fixed::overflow_error>([](){ fixed::detail::upscale<uint8_t>(2, 3, 100); }); // 100 -> 1000
	assert_throws<fixed::overflow_error>([](){ fixed::detail::upscale<uint8_t>(2, 4, 100); }); // 100 -> 10000
}

void test_utils_trim_comptime()
{
	bool loseless = false;
	assert((fixed::detail::trim<3, 2>(loseless, 234) == 23));
	static_assert((fixed::detail::trim<3, 3>(loseless, 1234) == 1234));
}

void test_utils_trim_runtime()
{
	bool loseless = false;
	static_assert((fixed::detail::trim(loseless, 3, 3, 1234) == 1234));
	assert_throws<fixed::invalid_argument>([&](){ fixed::detail::trim(loseless, 2, 3, 1234); });
}

void test_utils_fraction_order()
{
	assert(fixed::detail::fraction_order(1) == 1);
	assert(fixed::detail::fraction_order(9) == 1);
	assert(fixed::detail::fraction_order(10) == 2);
	assert(fixed::detail::fraction_order(99) == 2);
	assert(fixed::detail::fraction_order(100) == 3);
}

void test_utils_precision()
{
	static_assert(fixed::precision(fixed::p<0>()) == 0);
	static_assert(fixed::precision(fixed::p<1>()) == 1);
	static_assert(fixed::precision(fixed::p<2>()) == 2);
}

void test_utils_precision_constexpr()
{
	constexpr unsigned prec = fixed::precision(fixed::p<0>());
	static_assert(prec == 0);
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers)
