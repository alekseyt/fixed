#ifndef FIXED_TESTS_UTILS_HPP
#define FIXED_TESTS_UTILS_HPP

#include <cassert>
#include <concepts>
#include <functional>

template <typename Exception, std::invocable Test>
void assert_throws(Test t)
{
	try {
		std::invoke(t);
	}
	catch (const Exception &) {
		return;
	}
	catch (...) {}
	assert(false);
}

template <std::invocable Test>
void assert_no_throw(Test t)
{
	try {
		std::invoke(t);
	}
	catch (...) {
		assert(false);
	}
}

#endif // FIXED_TESTS_UTILS_HPP
